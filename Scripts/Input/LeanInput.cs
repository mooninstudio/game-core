﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lean.Touch;
using Moonin.Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Moonin.Input
{
    public static class LeanInput
    {
        public static IObservable<LeanFinger> FingerDownStream =
            Observable.FromEvent<LeanFinger>(h => LeanTouch.OnFingerDown += h, h => LeanTouch.OnFingerDown -= h);

        public static IObservable<LeanFinger> FingerUpdateStream =
            Observable.FromEvent<LeanFinger>(h => LeanTouch.OnFingerUpdate += h, h => LeanTouch.OnFingerUpdate -= h);

        public static IObservable<LeanFinger> FingerUpStream =
            Observable.FromEvent<LeanFinger>(h => LeanTouch.OnFingerUp += h, h => LeanTouch.OnFingerUp -= h);

        public static IObservable<LeanFinger> FingerTapStream =
            Observable.FromEvent<LeanFinger>(h => LeanTouch.OnFingerTap += h, h => LeanTouch.OnFingerTap -= h);

        public static IObservable<LeanFinger> FingerSwipeStream =
            Observable.FromEvent<LeanFinger>(h => LeanTouch.OnFingerSwipe += h, h => LeanTouch.OnFingerSwipe -= h);

        public static IObservable<List<LeanFinger>> GestureStream =
            Observable.FromEvent<List<LeanFinger>>(h => LeanTouch.OnGesture += h, h => LeanTouch.OnGesture -= h);

        public static IObservable<LeanFinger> FingerExpiredStream =
            Observable.FromEvent<LeanFinger>(h => LeanTouch.OnFingerExpired += h, h => LeanTouch.OnFingerExpired -= h);

        public static IObservable<Unit> AnyKeyPressedStream =>
            Observable.EveryUpdate().Where(_ => UnityEngine.Input.anyKeyDown).AsUnitObservable();

        public static IObservable<Unit> GetKeyPressedStream(Key key) => Observable.EveryUpdate()
            .Where(_ => Keyboard.current[key].wasPressedThisFrame).AsUnitObservable();

        public static IObservable<Unit> GetKeyReleasedStream(Key key) => Observable.EveryUpdate()
            .Where(_ => Keyboard.current[key].wasReleasedThisFrame).AsUnitObservable();

        public static IObservable<Unit> GetKeyHoldStream(Key key) => Observable.EveryUpdate()
            .Where(_ => Keyboard.current[key].isPressed).AsUnitObservable();

        public static IObservable<Vector2Int> SwipeDirectionStream = FingerSwipeStream.Select(GetSwipeDirection);

        public static IObservable<Vector2Int> GetDPadStream(Key left, Key right, Key up, Key down) =>
            Observable.EveryUpdate()
                .Select(_ => Keyboard.current.GetDpad(left, right, up, down));

        public static IObservable<Vector2Int> GetArrowsStream() =>
            GetDPadStream(Key.LeftArrow, Key.RightArrow, Key.UpArrow, Key.DownArrow);

        public static IObservable<Vector2Int> GetWSADStream() =>
            GetDPadStream(Key.A, Key.D, Key.W, Key.S);

        public static Vector2Int GetSwipeDirection(LeanFinger finger)
        {
            Vector2 swipe = finger.SwipeScaledDelta;
            if (swipe.x.Abs() > swipe.y.Abs())
                return swipe.x < 0 ? Vector2Int.left : Vector2Int.right;
            else
                return swipe.y < 0 ? Vector2Int.down : Vector2Int.up;
        }

        //	Gyroscope
        public static IObservable<float> DeviceRotationByAxisStream(Vector3 axis) => Observable.EveryUpdate()
            .Where(_ => SystemInfo.supportsGyroscope).Select(_ => DeviceRotation.GetAngleByDeviceAxis(axis));
    }
}