﻿using UnityEngine.InputSystem;

namespace Moonin.Input
{
	public class InputDeviceModel
	{
		public InputDevice Device { get; private set; }
		public ControllerType Type { get; private set; }
		public int DeviceId { get; private set; }

		private const int MAIN_DEVICE_ID = -1;

		public InputDeviceModel(InputDevice device)
		{
			Device = device;
			Type = device is Keyboard ? ControllerType.Keyboard : ControllerType.Gamepad;
			DeviceId = device.deviceId;
		}
	}
}