﻿using System;
using UniRx;

namespace Moonin.Input
{
	public enum ButtonState
	{
		Idle,
		Pressed,
		Held,
		Released,
	}
	public interface IObservableButton : IObservableControl<ButtonState>
	{
		IObservable<Unit> PressedStream { get; }
		IObservable<Unit> HeldStream { get; }
		IObservable<Unit> ReleasedStream { get; }
	}
	public class ObservableButton : ObservableControl<ButtonState>, IObservableButton
	{
		public IObservable<Unit> PressedStream => ValueStream.Where(x => x == ButtonState.Pressed).AsUnitObservable();
		public IObservable<Unit> HeldStream => ValueStream.Where(x => x == ButtonState.Held).AsUnitObservable();
		public IObservable<Unit> ReleasedStream => ValueStream.Where(x => x == ButtonState.Released).AsUnitObservable();
	}
}