using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Moonin.Input
{
	public class Controllers<TController>
		where TController : ControllerBase<TController>, new()
	{
		public IObservable<TController> ControllerConnectedStream => controllerConnectedSubject.AsObservable();
		public IObservable<TController> ControllerDisconnectedStream => controllerDisconnectedSubject.AsObservable();
		public List<TController> ConnectedControllers { get; private set; } = new List<TController>();
		public TController MainController { get; private set; }
		public TController AnyController { get; private set; }

		private Subject<TController> controllerConnectedSubject = new Subject<TController>();
		private Subject<TController> controllerDisconnectedSubject = new Subject<TController>();

		public Controllers()
		{
			var keyboardController = ControllerBase<TController>.From(Keyboard.current);
			MainController = ControllerBase<TController>.From(keyboardController);
			AnyController = ControllerBase<TController>.From(keyboardController);

			ConnectedControllers.Add(keyboardController);
			ConnectedControllers.AddRange(Gamepad.all.Select(ControllerBase<TController>.From));
			ConnectedControllers.ForEach(OnControllerConnected);

			InputSystem.onDeviceChange += OnDeviceChange;

			Observable.EveryUpdate().Subscribe(_ => UpdateState());
		}
		private void UpdateState()
		{
			ConnectedControllers.ForEach(x => x.Update());
			MainController.Update();
			AnyController.Update();
		}
		private void OnDeviceChange(InputDevice device, InputDeviceChange change)
		{
			switch (change)
			{
				case InputDeviceChange.Added:
				case InputDeviceChange.Reconnected:
					if (ConnectedControllers.Any(x => x.DeviceId == device.deviceId))
						break;
					var connected = ControllerBase<TController>.From(device);
					ConnectedControllers.Add(connected);
					OnControllerConnected(connected);
					break;
				case InputDeviceChange.Removed:
				case InputDeviceChange.Disconnected:
					if (ConnectedControllers.Remove(x => x.DeviceId == device.deviceId, out var disconnected))
						OnControllerDisconnected(disconnected);
					break;
			}
		}
		private void OnControllerConnected(TController controller)
		{
			Debug.Log("Device connected: " + controller.Type);

			if (ShouldLinkAsMain(controller.Type))
				ControllerBase<TController>.Rebind(MainController, controller);
			ControllerBase<TController>.Rebind(AnyController, ConnectedControllers);

			controllerConnectedSubject.OnNext(controller);
		}
		private void OnControllerDisconnected(TController controller)
		{
			Debug.Log("Device disconnected: " + controller.Type);

			if (MainController.DeviceId == controller.DeviceId)
				ControllerBase<TController>.Rebind(MainController,
					ConnectedControllers.FirstOrDefault(x => x.Type == ControllerType.Gamepad) ??
					ConnectedControllers.First(x => x.Type == ControllerType.Keyboard));
			ControllerBase<TController>.Rebind(AnyController, ConnectedControllers);

			controller.OnDisconnected();

			controllerDisconnectedSubject.OnNext(controller);
		}
		private bool ShouldLinkAsMain(ControllerType controller)
		{
			switch (MainController.Type)
			{
				case ControllerType.None: return true;
				case ControllerType.Keyboard: return controller == ControllerType.Gamepad;
				case ControllerType.Gamepad: return false;
				default: throw new UnknownEnumException<ControllerType>(MainController.Type);
			}
		}

		public void SetMainController(int controllerId)
		{
			var controller = ConnectedControllers.SingleOrDefault(x => x.DeviceId == controllerId);
			if (controller != null)
				ControllerBase<TController>.Rebind(MainController, controller);
		}
	}
}