﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace Moonin.Input
{
	public enum ControllerType
	{
		None,
		Keyboard,
		Gamepad,
	}
	public abstract class ControllerBase<TController>
		where TController : ControllerBase<TController>, new()
	{
		protected ControllerBase()
		{
			SetupBindings();
		}

		protected abstract void SetupBindings();
		
		public static TController From<TInputDevice>(TInputDevice device)
			where TInputDevice : InputDevice
		{
			return new TController().BindDevice(device);
		}
		public static TController From(TController backingController)
		{
			return new TController().BindController(backingController);
		}
		public static TController From(IEnumerable<TController> backingControllers)
		{
			return new TController().BindControllers(backingControllers);
		}
		public static TController From(params TController[] backingControllers)
		{
			return From((IEnumerable<TController>)backingControllers);
		}

		public static void Rebind<TInputDevice>(TController controller, TInputDevice device)
			where TInputDevice : InputDevice
		{
			controller.BindDevice(device);
		}
		public static void Rebind(TController controller, TController backingController)
		{
			controller.BindController(backingController);
		}
		public static void Rebind(TController controller, IEnumerable<TController> backingControllers)
		{
			controller.BindControllers(backingControllers);
		}
		public static void Rebind(TController controller, params TController[] backingControllers)
		{
			Rebind(controller, (IEnumerable<TController>)backingControllers);
		}

		public int DeviceId { get; private set; }
		public ControllerType Type { get; private set; }
		public IObservable<Unit> DisconnectedStream => disconnectedSubject.AsObservable();

		protected List<IDeviceBinding> bindingMappings = new List<IDeviceBinding>();
		protected List<Binding> bindings;

		private Subject<Unit> disconnectedSubject = new Subject<Unit>();

		private TController BindDevice<TInputDevice>(TInputDevice device)
			where TInputDevice : InputDevice
		{
			bindings = bindingMappings.Select(m => m.BindDevice(device)).ToList();
			DeviceId = device.deviceId;
			Type = device is Keyboard ? ControllerType.Keyboard : ControllerType.Gamepad;
			return (TController)this;
		}
		private TController BindController(TController controller)
		{
			bindings = bindingMappings.Select(m => m.BindController(controller)).ToList();
			DeviceId = controller.DeviceId;
			Type = controller.Type;
			return (TController)this;
		}
		private TController BindControllers(IEnumerable<TController> controllers)
		{
			bindings = bindingMappings.Select(m => m.BindMultipleControllers(controllers)).ToList();
			DeviceId = -1;
			Type = ControllerType.None;
			return (TController)this;
		}

		public void Update()
		{
			bindings.ForEach(b => b.Update());
		}
		public void OnDisconnected()
		{
			disconnectedSubject.OnNext();
		}

		protected IBinding<TValue, TControl> Bind<TControl, TValue>(Expression<Func<TController, TControl>> controlSelector)
			where TControl : IObservableControl<TValue>
		{
			var binding = new BindingMapping<TValue, TControl>((TController)this, controlSelector);

			bindingMappings.Add(binding);

			return binding;
		}
		protected IBinding<ButtonState> BindButton(Expression<Func<TController, IObservableButton>> controlSelector)
		{
			return Bind<IObservableButton, ButtonState>(controlSelector).As<ObservableButton>();
		}
		protected IBinding<Vector2> BindDpad(Expression<Func<TController, IObservableDpad>> controlSelector)
		{
			return Bind<IObservableDpad, Vector2>(controlSelector).As<ObservableDpad>();
		}

		protected ButtonState ReadValue(ButtonControl button)
		{
			if (button.wasPressedThisFrame) return ButtonState.Pressed;
			if (button.wasReleasedThisFrame) return ButtonState.Released;
			if (button.isPressed) return ButtonState.Held;
			return ButtonState.Idle;
		}
		protected Vector2 ReadValue(StickControl stick)
		{
			var raw = stick.ReadValue();
			return raw.magnitude < .1f ? Vector2.zero : raw;
		}
		protected Vector2 ReadValue(ButtonControl up, ButtonControl down, ButtonControl left, ButtonControl right)
		{
			return DpadControl.MakeDpadVector(
				up: up.isPressed,
				down: down.isPressed,
				left: left.isPressed,
				right: right.isPressed
			);
		}

		protected interface IDeviceBinding
		{
			Binding BindDevice<TDevice>(TDevice device) where TDevice : InputDevice;
			Binding BindController(TController controller);
			Binding BindMultipleControllers(IEnumerable<TController> controllers);
		}
		protected interface IBinding<TValue>
		{
			IBinding<TValue> From<TInputDevice>(Func<TInputDevice, TValue> getter)
				where TInputDevice : InputDevice;
			
			IBinding<TValue> Ignore<TInputDevice>() where TInputDevice : InputDevice;
		}
		protected interface IBinding<TValue, TControl>
			where TControl : IObservableControl<TValue>
		{
			IBinding<TValue> As<TConcreteControl>() where TConcreteControl : ObservableControl<TValue>, TControl, new();
		}
		protected class BindingMapping<TValue, TControl> : IBinding<TValue, TControl>, IBinding<TValue>, IDeviceBinding
			where TControl : IObservableControl<TValue>
		{
			private TController controller;
			private Expression<Func<TController, TControl>> controlSelector;
			private ObservableControl<TValue> control;
			private Dictionary<Type, Func<InputDevice, TValue>> valueBindings;

			public BindingMapping(TController controller, Expression<Func<TController, TControl>> controlSelector)
			{
				valueBindings = new Dictionary<Type, Func<InputDevice, TValue>>();

				this.controller = controller;
				this.controlSelector = controlSelector;
			}

			public IBinding<TValue> As<TConcreteControl>() where TConcreteControl : ObservableControl<TValue>, TControl, new()
			{
				var concreteControl = new TConcreteControl();
				AssignControl(concreteControl);
				control = concreteControl;
				return this;
			}
			private void AssignControl(TControl control)
			{
				var property = GetSelectedProperty();
				property.SetValue(controller, control, null);
			}
			private PropertyInfo GetSelectedProperty()
			{
				var memberExpression = (MemberExpression)controlSelector.Body;
				return (PropertyInfo)memberExpression.Member;
			}

			public IBinding<TValue> From<TInputDevice>(Func<TInputDevice, TValue> bindValue) where TInputDevice : InputDevice
			{
				valueBindings.Add(typeof(TInputDevice), device => bindValue(device as TInputDevice));
				return this;
			}
			
			public IBinding<TValue> Ignore<TInputDevice>() where TInputDevice : InputDevice
			{
				valueBindings.Add(typeof(TInputDevice), device => default);
				return this;
			}

			public Binding BindDevice<TDevice>(TDevice device)
				where TDevice : InputDevice
			{
				var deviceType = device.GetType();
				while (!valueBindings.ContainsKey(deviceType))
				{
					if (deviceType == typeof(InputDevice))
						throw new Exception($"No binding defined for device type: {deviceType}");
					deviceType = deviceType.BaseType;
				}

				return new Binding(() => control.OnNext(valueBindings[deviceType](device)));
			}
			public Binding BindController(TController controller)
			{
				var boundControl = (TControl)GetSelectedProperty().GetValue(controller);
				return new Binding(() => control.OnNext(boundControl.Value));
			}
			public Binding BindMultipleControllers(IEnumerable<TController> controllers)
			{
				var boundControls = controllers.Select(controller => (TControl)GetSelectedProperty().GetValue(controller));
				return new Binding(() => boundControls.ForEach(boundControl => control.OnNext(boundControl.Value)));
			}
		}
		protected class Binding
		{
			private Action updateValue;

			public Binding(Action updateValue)
			{
				this.updateValue = updateValue;
			}

			public void Update()
			{
				updateValue();
			}
		}

		protected Vector2 GetWsadDPad(Keyboard keyboard)
		{
			return DpadControl.MakeDpadVector(
				keyboard.wKey.isPressed,
				keyboard.sKey.isPressed,
				keyboard.aKey.isPressed,
				keyboard.dKey.isPressed);
		}
	}
}