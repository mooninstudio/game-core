using System;

public class UnknownEnumException<T> : Exception where T : Enum
{
    public UnknownEnumException(T item) : base(GetMessage(item)) { }
    private static string GetMessage(T item)
    {
        return $"Unknown {typeof(T)}: {item}";
    }
}