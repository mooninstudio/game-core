public interface IMonoInitializable
{
    void Initialize();
}