using UnityEngine;
using System.Linq;

public static class MonoInitializer
{
    public static void Initialize(){
        Resources.FindObjectsOfTypeAll<MonoBehaviour>().OfType<IMonoInitializable>().ToList()
            .ForEach(mono => mono.Initialize());
    }
}