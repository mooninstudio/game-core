﻿using UnityEngine;

public class UIToWorldConverter : MonoBehaviour, IUIToWorldConverter
{
    [SerializeField] private Camera worldElementsCamera;

    public static IUIToWorldConverter Instance;

    public void Initialize()
    {
        Instance = this;
    }

    public Vector3 CameraToWorldSpace(Vector2 cameraSpacePosition, Camera camera)
    {
        return worldElementsCamera.ScreenToWorldPoint(camera.WorldToScreenPoint(cameraSpacePosition));
    }
}

public interface IUIToWorldConverter
{
    Vector3 CameraToWorldSpace(Vector2 cameraSpacePosition, Camera camera);
}
