namespace Moonin.State
{
    public static class PropertyExtensions
    {
        public static void Add(this Property<int> prop, int value) => prop.Set(prop.Value + value);
        public static void Add(this Property<float> prop, float value) => prop.Set(prop.Value + value);
    }
}