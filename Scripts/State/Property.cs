﻿using System;

namespace Moonin.State
{
    public class Property<T> : ChangablePropertyBase<T>
    {
        public Property(T defaults = default) : base(defaults)
        {
        }

        public Property(Func<T> getDefaultValue) : base(getDefaultValue)
        {
        }

        public void Set(T newValue) => ChangeValue(newValue);
    }
}