﻿using System;

public abstract class PropertyBase<T> : IObservable<T>
{
    public T Value { get; protected set; }
    public bool HasValue => Value != null;
    public abstract IDisposable Subscribe(IObserver<T> observer);
}