﻿using System;
using UniRx;

namespace Moonin.State
{
    public class RelationProperty<T> : PropertyBase<T>
    {
        private IObservable<T> relation;

        public override IDisposable Subscribe(IObserver<T> observer) => relation.Subscribe(observer);

        public RelationProperty(IObservable<T> relation)
        {
            this.relation = relation;
            relation.Subscribe(x => Value = x);
        }
    }
}