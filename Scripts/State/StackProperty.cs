﻿using System;
using System.Collections.Generic;
using UniRx;

public class StackProperty<T> : Stack<T>, IObservable<IEnumerable<T>>
{
    private Subject<IEnumerable<T>> subject = new Subject<IEnumerable<T>>();
    public IDisposable Subscribe(IObserver<IEnumerable<T>> observer) => subject.Subscribe(observer);

    public new void Push(T item)
    {
        base.Push(item);
        subject.OnNext(this);
    }

    public new T Pop()
    {
        var item = base.Pop();
        subject.OnNext(this);
        return item;
    }
}