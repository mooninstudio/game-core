using UnityEngine;

namespace Moonin.Controller2D
{
	[CreateAssetMenu(
		fileName = nameof(PlayerControllerSettings),
		menuName = ScriptableObjectConsts.ProjectPrefix + nameof(PlayerControllerSettings))]
	[System.Serializable]
	public class PlayerControllerSettings : ScriptableObject
	{
		[SerializeField] private float maxJumpHeight = 4f;
		[SerializeField] private float minJumpHeight = 1f;
		[SerializeField] private float timeToJumpApex = .4f;
		[SerializeField] private float moveSpeed = 6f;
		[SerializeField] private float wallSlideSpeedMax = 6f;
		[Header("Wall jump")] [SerializeField] private Vector2 wallJumpClimb;
		[SerializeField] private Vector2 wallJumpOff;
		[SerializeField] private Vector2 wallJumpLeap;
		[SerializeField] private float wallStickTime = .25f;

		public float MaxJumpHeight => maxJumpHeight;
		public float MinJumpHeight => minJumpHeight;
		public float TimeToJumpApex => timeToJumpApex;
		public float MoveSpeed => moveSpeed;
		public float WallSlideSpeedMax => wallSlideSpeedMax;

		public Vector2 WallJumpClimb => wallJumpClimb;
		public Vector2 WallJumpOff => wallJumpOff;
		public Vector2 WallJumpLeap => wallJumpLeap;
		public float WallStickTime => wallStickTime;
	}
}