using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonin.Controller2D
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class RaycastController : MonoBehaviour
    {
        [SerializeField] protected LayerMask collisionMask;

        public Vector2 Position => transform.position;

        protected BoxCollider2D box;
        protected RaycastOrigins raycastOrigins;
        protected Vector2Int raycastResolution;
        protected float horizontalRaySpacing;
        protected float verticalRaySpacing;

        protected const float skinWidth = .015f;
        protected const float raysSpacing = .25f;

        protected virtual void Awake()
        {
            box = GetComponent<BoxCollider2D>();
            CalculateRaySpacing();
        }

        private void CalculateRaySpacing()
        {
            var bounds = box.bounds;
            bounds.Expand(skinWidth * -2);

            var boundsWidth = bounds.size.x;
            var boundsHeight = bounds.size.y;

            raycastResolution.x = Mathf.RoundToInt(boundsHeight / raysSpacing);
            raycastResolution.y = Mathf.RoundToInt(boundsWidth / raysSpacing);

            horizontalRaySpacing = bounds.size.y / (raycastResolution.x - 1);
            verticalRaySpacing = bounds.size.x / (raycastResolution.y - 1);
        }

        protected void UpdateRaycastOrigins()
        {
            var bounds = box.bounds;
            bounds.Expand(skinWidth * -2);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
        }


        protected struct RaycastOrigins
        {
            public Vector2 topLeft;
            public Vector2 topRight;
            public Vector2 bottomLeft;
            public Vector2 bottomRight;
        }

    }
}