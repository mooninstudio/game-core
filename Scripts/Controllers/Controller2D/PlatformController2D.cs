using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonin.Controller2D
{
    public class PlatformController2D : RaycastController
    {
        [SerializeField] private Vector2 testMove;
        [SerializeField] private LayerMask passengerMask;

        private HashSet<Transform> movedPassengers = new HashSet<Transform>();
        private List<PassengerMovement> passengerMovement = new List<PassengerMovement>();
        private Dictionary<Transform, Controller2D> passengerControllers = new Dictionary<Transform, Controller2D>();

        protected override void Awake()
        {
            base.Awake();
        }

        private void Update()
        {
            movedPassengers.Clear();
            passengerMovement.Clear();

            UpdateRaycastOrigins();
            var velocity = testMove * Time.deltaTime;

            CalculatePassengersMovement(velocity);
            MovePassengers(true);
            transform.Translate(velocity);
            MovePassengers(false);
        }

        private void MovePassengers(bool beforeMovePlatform)
        {
            foreach (var passenger in passengerMovement)
            {
                if (!passengerControllers.ContainsKey(passenger.Transform))
                    passengerControllers.Add(passenger.Transform, passenger.Transform.GetComponent<Controller2D>());
                if (passenger.MoveBeforePlatform == beforeMovePlatform)
                    passengerControllers[passenger.Transform].Move(passenger.Velocity, passenger.OnPlatform);
            }
        }

        private void CalculatePassengersMovement(Vector3 velocity)
        {
            var directionX = velocity.x.Sign();
            var directionY = velocity.y.Sign();

            if (velocity.y != 0)
            {
                var rayLength = velocity.y.Abs() + skinWidth;

                for (int i = 0; i < raycastResolution.y; i++)
                {
                    var rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                    rayOrigin += Vector2.right * (verticalRaySpacing * i);
                    var hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);

                    if (hit && hit.distance != 0)
                    {
                        if (!movedPassengers.Contains(hit.transform))
                        {
                            movedPassengers.Add(hit.transform);
                            var pushX = (directionY == 1) ? velocity.x : 0;
                            var pushY = velocity.y - (hit.distance - skinWidth) * directionY;

                            passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY),
                                directionY == 1, true));
                        }
                    }
                }
            }

            if (velocity.x != 0)
            {
                var rayLength = velocity.x.Abs() + skinWidth;

                for (int i = 0; i < raycastResolution.x; i++)
                {
                    var rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                    rayOrigin += Vector2.up * (verticalRaySpacing * i);
                    var hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passengerMask);

                    if (hit && hit.distance != 0)
                    {
                        if (!movedPassengers.Contains(hit.transform))
                        {
                            movedPassengers.Add(hit.transform);
                            var pushX = velocity.x - (hit.distance - skinWidth) * directionX;
                            var pushY = -skinWidth;

                            passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), false,
                                true));
                        }
                    }
                }
            }

            if (directionY == -1 || velocity.y == 0 && velocity.x != 0)
            {
                var rayLength = skinWidth * 2;

                for (int i = 0; i < raycastResolution.y; i++)
                {
                    var rayOrigin = raycastOrigins.topLeft + Vector2.right * (verticalRaySpacing * i);
                    var hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);

                    if (hit && hit.distance != 0)
                    {
                        if (!movedPassengers.Contains(hit.transform))
                        {
                            movedPassengers.Add(hit.transform);
                            var pushX = velocity.x;
                            var pushY = velocity.y;

                            passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), true,
                                false));
                        }
                    }
                }
            }
        }

        private struct PassengerMovement
        {
            public Transform Transform { get; set; }
            public Vector3 Velocity { get; set; }
            public bool OnPlatform { get; set; }
            public bool MoveBeforePlatform { get; set; }

            public PassengerMovement(Transform transform, Vector3 velocity, bool onPlatform, bool moveBeforePlatform)
            {
                Transform = transform;
                Velocity = velocity;
                OnPlatform = onPlatform;
                MoveBeforePlatform = moveBeforePlatform;
            }
        }
    }
}