using System.Linq;
using UnityEngine;

namespace Moonin.Solvers.FABRIK
{
    public class FABRIKSolver
    {
        private int iterations;
        private Vector3[] nodes;
        private float[] distances;
        
        public FABRIKSolver(int iterations, Vector3[] nodes)
        {
            this.iterations = iterations;
            this.nodes = nodes;
            this.distances = nodes
                .Pairwise()
                .Select(p => Vector3.Distance(p.Item1, p.Item2))
                .ToArray();
        }
        
        public void Solve(Vector3 target, Vector3 origin, float gravity)
        {
            for (var n = 1; n < nodes.Length; n++)
            {
                nodes[n] += Vector3.down * gravity * Time.deltaTime;    
            }
            
            for (var i = 0; i < iterations; i++)
            {
                nodes[0] = target;
                for (var n = 1; n < nodes.Length; n++)
                {
                    var toNextNode = (nodes[n] - nodes[n - 1]).normalized;
                    nodes[n] = nodes[n - 1] + toNextNode * distances[n - 1];
                }

                nodes[nodes.Length - 1] = origin;
                for (var n = nodes.Length - 2; n >= 0; n--)
                {
                    var toPrevNode = (nodes[n] - nodes[n + 1]).normalized;
                    nodes[n] = nodes[n + 1] + toPrevNode * distances[n];
                }
            }
        }
    }
}