﻿using UnityEngine;

namespace Moonin.TopDown3D
{
    public abstract class ActorController : MonoBehaviour
    {
        public Vector2 Position => _actor.Position;
        public Vector3 Position3D => _actor.Position3D;
        public float Rotation => _actor.Rotation;
        
        protected Actor _actor;

        private void Awake() => OnAwake();
        private void Start() => OnStart();

        protected virtual void OnAwake()
        {
            _actor = GetComponentInChildren<Actor>();
            _actor.Register(transform.position);
        }
        protected virtual void OnStart() { }

        private void FixedUpdate() => OnFixedUpdate();

        protected virtual void OnFixedUpdate()
        {
            transform.position = _actor.Position3D;
            transform.rotation = _actor.Rotation3D;
        }

        public T GetActorComponent<T>() where T : Component => _actor.GetComponent<T>();

        private void Reset()
        {
            _actor = GetComponentInChildren<Actor>() ?? Actor.Instantiate(transform);
        }
    }
}
