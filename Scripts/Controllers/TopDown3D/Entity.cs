using UnityEngine;

namespace Moonin.TopDown3D
{
    public class Entity : MonoBehaviour
    {
        [SerializeField] private EntityGraphics _graphics;

        public Vector2 Position => transform.position;
        public float Rotation => transform.eulerAngles.z;
        
        protected virtual void Update()
        {
            _graphics.UpdateTransform(
                TopDownUtils.To3D(Position),
                TopDownUtils.To3D(Rotation));
        }
    }
}