using UnityEngine;

namespace Moonin.TopDown3D
{
    public static class TopDownUtils
    {
        public static Vector2 To2D(this Vector3 position)
        {
            return new Vector2(position.x, position.z);
        }
        
        public static Vector3 To3D(this Vector2 position)
        {
            return new Vector3(position.x, 0, position.y);
        }
        
        public static Quaternion To2D(this Quaternion rotation)
        {
            var euler3D = rotation.eulerAngles;
            return Quaternion.Euler(0, 0, -euler3D.y);
        }
        
        public static float ToAngle2D(this Quaternion rotation)
        {
            return -rotation.eulerAngles.y;
        }
        
        public static Quaternion To3D(this Quaternion rotation)
        {
            var euler2D = rotation.eulerAngles;
            return To3D(euler2D.z);
        } 
        
        public static Quaternion To3D(this float angle)
        {
            return Quaternion.Euler(0, -angle, 0);
        }
    }
}