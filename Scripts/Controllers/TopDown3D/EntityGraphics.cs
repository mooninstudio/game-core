using UnityEngine;

namespace Moonin.TopDown3D
{
    public class EntityGraphics : MonoBehaviour
    {
        public void UpdateTransform(Vector3 position, Quaternion rotation)
        {
            transform.SetPositionAndRotation(position, rotation);
        }
    }
}