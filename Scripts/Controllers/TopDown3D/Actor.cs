using Moonin.Extensions;
using UnityEngine;

namespace Moonin.TopDown3D
{
    public class Actor : MonoBehaviour
    {
        public Vector2 Position => transform.position;
        public float Rotation => transform.eulerAngles.z;
        public Vector3 Position3D =>  ActorUtils.To3D(transform.position);
        public Quaternion Rotation3D =>  ActorUtils.To3D(transform.rotation);
        
        public void Register(Vector3 position)
        {
            if (actorsRoot == null)
                actorsRoot = new GameObject("actors_root").transform;
            
            transform.SetParent(actorsRoot);
            transform.position = ActorUtils.To2D(position);
        }
        
        private static Transform actorsRoot;
        public static Actor Instantiate(Transform target)
        {
            var actorGameObject = new GameObject($"{target.name}_actor",
                typeof(Actor));

            actorGameObject.transform.SetParent(target);
            
            var actor = actorGameObject.GetComponent<Actor>();
            
            return actor;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            switch (GetComponent<Collider2D>())
            {
                case BoxCollider2D box: DrawBox(box); break;
                case CircleCollider2D circle: DrawCircle(circle); break;
                case CapsuleCollider2D capsule: DrawCapsule(capsule); break;
            }
        }
        private void DrawBox(BoxCollider2D box)
        {
            GizmosExt.DrawSquare(ActorUtils.To3D(box.transform.position), box.size, -transform.rotation.eulerAngles.z);
        }
        private void DrawCircle(CircleCollider2D circle)
        {
            GizmosExt.DrawCircle(ActorUtils.To3D(transform.position), circle.radius);   
        }
        private void  DrawCapsule(CapsuleCollider2D capsule)
        {
            GizmosExt.DrawCircle(ActorUtils.To3D(transform.position), capsule.size.x);
        }
    }

    public static class ActorUtils
    {
        public static Vector2 To2D(Vector3 position)
        {
            return new Vector2(position.x, position.z);
        }
        
        public static Vector3 To3D(Vector2 position)
        {
            return new Vector3(position.x, 0, position.y);
        }
        
        public static Quaternion To2D(Quaternion rotation)
        {
            var euler3D = rotation.eulerAngles;
            return Quaternion.Euler(0, 0, -euler3D.y);
        }
        
        public static Quaternion To3D(Quaternion rotation)
        {
            var euler2D = rotation.eulerAngles;
            return Quaternion.Euler(0, -euler2D.z, 0);
        }
    }
}
