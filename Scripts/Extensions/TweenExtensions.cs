using System;
using DG.Tweening;
using UniRx;

namespace Moonin.DOTween.Extensions
{
    public static class TweenExtensions
    {
        /// <summary>
        /// Plays as observable. Emits Unit on end of every loop.
        /// </summary>
        public static IObservable<Unit> PlayAsObservable<T>(this T tween)
            where T : Tween
        {
            return Observable.Create<Unit>(observer =>
            {
                tween
                    .OnStepComplete(() => observer.OnNext(Unit.Default))
                    .OnComplete(observer.OnCompleted)
                    .Play();
                return Disposable.Empty;
            });
        }
    }
}