﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using UniRx.Diagnostics;
using System.Threading.Tasks;
using UnityEngine.Events;

public static class ObservableExt
{
    public static IObservable<T> NotNull<T>(this IObservable<T> source)
    {
        return source.Where(x => x != null);
    }
    public static IObservable<T> NotNull<T>(this IObservable<T> source, object obj)
    {
        return source.Where(_ => obj != null);
    }
    public static IObservable<Unit> AsUnitObservable(this Action action)
    {
        return Observable.Create<Unit>(observer =>
        {
            action();
            observer.OnNext(Unit.Default);
            observer.OnCompleted();
            return Disposable.Empty;
        });
    }
    public static IObservable<Unit> ToMainThreadObservable(this Task task) => WaitForTask(task).ToObservable();
    public static IObservable<T> ToMainThreadObservable<T>(this Task<T> task) => WaitForTask(task).ToObservable().Select(_ => task.Result);
    private static IEnumerator WaitForTask(Task task)
    {
        yield return new WaitUntil(() => task.IsCompleted);
    }

    public static IObservable<Unit> EveryRandom(float minSeconds, float maxSeconds) => RandomWait(minSeconds, maxSeconds).ToObservable(true);
    private static IEnumerator RandomWait(float minSeconds, float maxSeconds)
    {
        while (true)
            yield return new WaitForSeconds(UnityEngine.Random.Range(minSeconds, maxSeconds));
    }
    public static IObservable<T> DelaySeconds<T>(this IObservable<T> source, float seconds)
    {
        return source.Delay(TimeSpan.FromSeconds(seconds));
    }
    public static IObservable<T> DelaySubscriptionSeconds<T>(this IObservable<T> source, float seconds)
    {
        return source.DelaySubscription(TimeSpan.FromSeconds(seconds));
    }
    public static IDisposable Subscribe(this IObservable<Unit> source, Action observer)
    {
        return source.Subscribe(_ => observer());
    }
    public static IDisposable Subscribe<T>(this IObservable<Unit> source, Func<T> observer)
    {
        return source.Subscribe(_ => observer());
    }
    public static IDisposable SubscribeDebug<T>(this IObservable<T> source, string message)
    {
        return source.Subscribe(_ => Debug.Log(message));
    }
    public static IDisposable SubscribeDebug<T>(this IObservable<T> source, Func<T, string> message)
    {
        return source.Subscribe(x => Debug.Log(message(x)));
    }
    public static IObservable<T> ToObservable<T>(this UnityEvent<T> source)
    {
        return Observable.FromEvent<T>(
            x => source.AddListener(a => x(a)), 
            x => source.RemoveListener(a => x(a))
        );
    }

    public static IObservable<float> Timer(float time)
    {
        var timer = 0f;
        return Observable.EveryUpdate()
            .Select(_ => (timer = Mathf.MoveTowards(timer, time, Time.deltaTime)) / time)
            .TakeWhile(x => x > 0);
    }

    public static void OnNext(this Subject<Unit> subject)
    {
        subject.OnNext(Unit.Default);
    }
}
