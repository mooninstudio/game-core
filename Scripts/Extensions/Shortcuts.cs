using System;
using UniRx;

public static class Shortcuts
{
    public static IObservable<Unit> AsUnitObservable(Action action)
    {
        return action.AsUnitObservable();
    }
}