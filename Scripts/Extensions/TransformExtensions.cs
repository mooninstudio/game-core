using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    public static List<T> GetComponentsInCloseChildren<T>(this Transform transform, bool includeInactive) where T : Component
    {
        var components = new List<T>();
        foreach (Transform child in transform)
        {
            if (!includeInactive && !child.gameObject.activeSelf)
                continue;
            if(child.TryGetComponent<T>(out var comp))
                components.Add(comp);
        }
        return components;
    }

    public static void DestroyChildren(this Transform transform)
    {
        foreach (Transform child in transform.GetComponentsInCloseChildren<Transform>(true))
        {
            Object.Destroy(child.gameObject);
        }
    }
    public static void DestroyChildrenImmediate(this Transform transform)
    {
        foreach (Transform child in transform.GetComponentsInCloseChildren<Transform>(true))
        {
            Object.DestroyImmediate(child.gameObject);
        }
    }
}