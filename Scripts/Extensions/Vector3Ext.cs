﻿using System;
using System.Linq;
using UnityEngine;

public static class Vector3Ext
{
	public static Vector3 MidPoint(params Vector3[] vectors)
	{
		if(!vectors.Any())
            return default;
			
        var minX = vectors.Min(v => v.x);
		var minY = vectors.Min(v => v.y);
		var minZ = vectors.Min(v => v.z);
		var maxX = vectors.Max(v => v.x);
		var maxY = vectors.Max(v => v.y);
		var maxZ = vectors.Max(v => v.z);

		return new Vector3((maxX - minX) * .5f + minX, (maxY - minY) * .5f + minY, (maxZ - minZ) * .5f + minZ);
	}
	public static Vector3 ArcLerp(Vector3 from, Vector3 to, Vector3 bend, float t)
	{
		return Vector3.Lerp(Vector3.Lerp(from, bend, t), Vector3.Lerp(bend, to, t), t);
	}
	public static Vector3 Modify(this Vector3 target, Func<float, float> modX = null, Func<float, float> modY = null, Func<float, float> modZ = null)
	{
		return new Vector3()
		{
			x = modX != null ? modX(target.x) : target.x,
			y = modY != null ? modY(target.y) : target.y,
			z = modZ != null ? modZ(target.z) : target.z,
		};
	}
	public static Vector3 ToVector3XZ(this Vector2 target, float y = 0)
	{
        return new Vector3(target.x, y, target.y);
    }
}
