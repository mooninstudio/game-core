﻿using System;
using System.IO;
using System.Linq;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Moonin.Extensions
{
    public static class ScriptableObjectExt
    {
        public static string ReadAssetGuid(this Object target)
        {
#if UNITY_EDITOR
            var assetPath = AssetDatabase.GetAssetPath(target);
            if (string.IsNullOrEmpty(assetPath))
                throw new Exception($"Instance is not an asset");

            var metaPath = AssetDatabase.GetTextMetaFilePathFromAssetPath(assetPath);
            using var streamReader = File.OpenText(metaPath);
            var assetId = new string(streamReader
                .ReadToEnd()
                .Split("guid: ")[1]
                .Take(32)
                .ToArray());

            return assetId;
#else
            throw new Exception("Can't access asset meta outside editor");
#endif
        }
    }
}