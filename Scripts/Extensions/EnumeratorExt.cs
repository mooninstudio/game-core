﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public static class EnumeratorExt
{
	public static IEnumerator ActionOverTime(Action<float> action, float time, Action onComplete = null, float startFrom = 0, bool loop = false)
	{
		float value = startFrom;
        do
        {
            do
            {
                value = Mathf.MoveTowards(value, 1, Time.deltaTime / time);
                action?.Invoke(value);
                yield return null;
            }
            while (value != 1);
            onComplete?.Invoke();
            value = 0;
        }
        while (loop);
    }
	public static IDisposable Subscribe(this IEnumerator enumerator, Action onEmit = null)
	{
        return enumerator.ToObservable().Subscribe(_ => onEmit?.Invoke());
    }
    public static IEnumerator Delay(Action action, float delay)
    {
        if(delay > 0)
            yield return new WaitForSeconds(delay);
        action();
    }
	public static IEnumerator Delay(this IEnumerator source, float delay)
	{
		if(delay > 0)
			yield return new WaitForSeconds(delay);
        yield return source;
    }
	public static IEnumerator ProcessCoroutine(float duration, Action<float> onProcess = null, Action onComplete = null, float progressFrom = 0f)
    {
        var t = progressFrom;
        while (t != 1)
        {
            t = Mathf.MoveTowards(t, 1f, Time.deltaTime / duration);
            onProcess?.Invoke(t);
            yield return null;
        }
        onComplete?.Invoke();
    }
}
