﻿using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#else
using System;
#endif

namespace Moonin.Extensions
{
    public static class AssetDatabaseExt
    {
        public static IEnumerable<T> FindAssetsByType<T>() where T : UnityEngine.Object
        {
#if UNITY_EDITOR
            var guids = AssetDatabase.FindAssets($"t:{typeof(T)}");
            foreach (var t in guids)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath( t );
                var asset = AssetDatabase.LoadAssetAtPath<T>( assetPath );
                if( asset != null )
                    yield return asset;
            }

#else
            throw new Exception("Can't access AssetDatabase outside Editor");
#endif
        }

    }
}