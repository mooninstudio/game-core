using UnityEngine;

public static class DebugHelpers
{
    public static void Break(string message = null)
    {
        if(message != null)
            Debug.Log(message);
        Debug.Break();
    }
}