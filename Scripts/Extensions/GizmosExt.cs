using UnityEngine;

namespace Moonin.Extensions
{
    public static class GizmosExt
    {
        public static void DrawCircle(Vector3 position, Vector3 up, float radius, int resolution = 100)
        {
            var dir = 
                Vector3.RotateTowards(
                    up, 
                    -up,
                    90 * Mathf.Deg2Rad,
                    0)
                    .normalized * radius;
            var step = 360f / (resolution - 1);
            for (var i = 0; i < resolution; i++)
            {
                var nextDir = Quaternion.AngleAxis(step, up) * dir;
                Gizmos.DrawLine(position + dir, position + nextDir);
                dir = nextDir;
            }
        }
        
        public static void DrawCircle(Vector3 position, float radius, int resolution = 100)
        {
            var dir = Vector3.left * radius;
            var step = 360f / (resolution - 1);
            for (var i = 0; i < resolution; i++)
            {
                var nextDir = Quaternion.Euler(0,step, 0) * dir;
                Gizmos.DrawLine(position + dir, position + nextDir);
                dir = nextDir;
            }
        }
        
        public static void DrawEllipse(Vector2 position, float radius, float tilt, int resolution = 100)
        {
            var dir = Vector2.left;
            var step = 360f / (resolution - 1);
            for (var i = 0; i < resolution; i++)
            {
                var nextDir = dir.Rotate(step);
                Gizmos.DrawLine(
                    position + dir.Tilt(tilt) * radius, 
                    position + nextDir.Tilt(tilt) * radius);
                dir = nextDir;
            }
        }
        
        public static void DrawSquare(Vector3 position, Vector3 up, Vector2 size, float angle = 0f)
        {
            var right = up == Vector3.up ? 
                Vector3.right :
                Quaternion.FromToRotation(Vector3.up, up) * Vector3.right;
            right = Quaternion.AngleAxis(angle, up) * right;
            var forward = Vector3.Cross(up, right).normalized;

            var p1 = position - right * .5f * size.x - forward * .5f * size.y;
            var p2 = position - right * .5f * size.x + forward * .5f * size.y;
            var p3 = position + right * .5f * size.x + forward * .5f * size.y;
            var p4 = position + right * .5f * size.x - forward * .5f * size.y;
            
            Gizmos.DrawLine(p1, p2);
            Gizmos.DrawLine(p2, p3);
            Gizmos.DrawLine(p3, p4);
            Gizmos.DrawLine(p4, p1);
        }

        public static void DrawSquare(Vector3 position, Vector2 size, float angle = 0f)
        {
            DrawSquare(position, Vector3.up, size, angle);
        }
    }
}