﻿using System;
using UnityEngine;

public static class MathfExtensions
{
    public static float MapValue(this float value, float from, float to) => 
        Map(value, 0, 1, from, to);
    public static float Map(this float value, float from1, float to1, float from2, float to2)
    {
        return value.Clamp(from1, to1).MapUnclamped(from1, to1, from2, to2);
    }
    public static float MapUnclamped(this float value, float from1, float to1, float from2, float to2)
	{
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
	}
    public static int Clamp(this int value, int min, int max) => value < min ? min : value >= max ? max-1 : value;
    public static float Clamp01(this float value) => Clamp(value, 0, 1);
    public static float Clamp(this float value, float min, float max)
    {
        return value < min ? min : value > max ? max : value;
    }
    public static float Abs(this float value)
    {
        return value < 0 ? -value : value;
    }
    public static int Abs(this int value)
    {
        return value < 0 ? -value : value;
    }
    public static float Sign(this float value)
    {
        return value < 0 ? -1 : 1;
    }
    public static int SignInt(this float value)
    {
        return value < 0 ? -1 : 1;
    }
    public static float Max(this float value, float maxValue) => Math.Min(value, maxValue);
    public static int Max(this int value, int maxValue) => Math.Min(value, maxValue);
    public static float Min(this float value, float minValue) => Math.Max(value, minValue);
    public static int Min(this int value, int minValue) => Math.Max(value, minValue);
    public static float Sqr(this float value) => value * value;
    public static int Sqr(this int value) => value * value;
    public static float Sqrt(this float value) => (float)Math.Sqrt(value);
    public static float Sqrt(this int value) => (float)Math.Sqrt(value);
    public static float Sigmoid(float value) {
        return 1.0f / (1.0f + (float) Math.Exp(-value));
    }
    public static float Round(this float value, int digits)
    {
        return (float)Math.Round(value, digits);
    }

    public static float ToZero(this float value, float delta)
    {
        return (value - delta).Min(0);
    }

    public static float Circumference(float radius)
    {
        return 2 * Mathf.PI * radius;
    }
}
