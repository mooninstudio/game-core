using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class EditorHelpers
{
    public static IEnumerable<T> FindAssetsByType<T>() where T : UnityEngine.Object
    {
#if UNITY_EDITOR
        string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                yield return asset;
            }
        }
#else
        yield break;
#endif
    }
}