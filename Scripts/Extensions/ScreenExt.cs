﻿using UnityEngine;

namespace Moonin.Extensions
{
    public static class ScreenExt
    {
        public static Vector2 Normalise(Vector2 position)
        {
            return new Vector2(position.x / Screen.width, position.y / Screen.height);
        }
    }
}