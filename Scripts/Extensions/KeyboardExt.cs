﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Moonin.Extensions
{
    public static class KeyboardExt
    {
        public static Vector2Int GetDpad(this Keyboard keyboard, Key left, Key right, Key up, Key down)
        {
            return Vector2Ext.FromBools(
                left: keyboard[left].isPressed,
                right: keyboard[right].isPressed,
                up: keyboard[up].isPressed,
                down: keyboard[down].isPressed);
        }
    }
}