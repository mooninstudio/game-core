using UnityEngine;

public static class ObjectExtensions
{
    public static T Instantiate<T>(this Object obj, T prefab, Vector3 position)
        where T : Object
    {
        return Object.Instantiate(prefab, position, Quaternion.identity);
    }
    public static T Instantiate<T>(this Object obj, T prefab, Vector3 position, Transform parent)
        where T : Object
    {
        return Object.Instantiate(prefab, position, Quaternion.identity, parent);
    }
}