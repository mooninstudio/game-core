﻿using System;
using System.Linq;
using System.Collections.Generic;

public static class IEnumerableExtensions
{
    private static Random rnd = new Random();

    public static IEnumerable<T> NotNull<T>(this IEnumerable<T> enumerable)
    {
        return enumerable.Where(x => x != null);
    }
    public static T Find<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
    {
        foreach (T item in enumerable)
        {
            if (predicate(item)) return item;
        }
        return default(T);
    }
    public static bool TryFind<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate, out T result)
    {
        result = default(T);
        foreach (T item in enumerable)
        {
            if (predicate(item)) { result = item; return true; }
        }
        return false;
    }
    public static T GetNextLoop<T>(this T[] array, int current)
    {
        return array[(++current) % array.Length];
    }
    public static T GetPrevLoop<T>(this T[] array, int current)
    {
        return array[--current < 0 ? array.Length - 1 : current];
    }
    public static T RandomElement<T>(this IEnumerable<T> collection)
    {
        return collection.ElementAt(rnd.Next(0, collection.Count()));
    }
    public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
    {
        foreach (var item in collection)
            action.Invoke(item);
    }
    public static void ForEach<T>(this IEnumerable<T> collection, Action<T, int> action)
    {
        int i = 0;
        foreach (var item in collection)
            action.Invoke(item, i++);
    }
    public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection)
    {
        return collection == null || !collection.Any();
    }
    public static IEnumerable<T> RemoveFirst<T>(this IEnumerable<T> collection)
    {
        return collection.Skip(1);
    }
    public static IEnumerable<T> RemoveLast<T>(this IEnumerable<T> collection)
    {
        return collection.Take(collection.Count() - 1);
    }
    public static void Enqueue<T>(this Queue<T> queue, IEnumerable<T> elements)
    {
        elements.ForEach(x => queue.Enqueue(x));
    }
    public static IEnumerable<(T, T)> Pairwise<T>(this IEnumerable<T> source)
    {
        var previous = default(T);
        using (var it = source.GetEnumerator())
        {
            if (it.MoveNext())
                previous = it.Current;

            while (it.MoveNext())
                yield return (previous, previous = it.Current);
        }
    }
    public static void Prepend<T>(this List<T> list, T item)
    {
        list.Insert(0, item);
    }

    public static bool TryPop<T>(this List<T> list, out T item, int? index = null)
    {
        if(!list.Any())
        {
            item = default;
            return false;
        }
        item = index.HasValue ? list[index.Value] : list.Last();
        list.RemoveAt(index.HasValue ? index.Value : list.Count - 1);
        return true;
    }
    public static T Pop<T>(this List<T> list, int? index = null)
    {
        if(!list.TryPop(out var item, index))
            throw new Exception("List cannot be empty");

        return item;
    }

    public static bool Remove<T>(this List<T> source, Predicate<T> predicate) => source.Remove(predicate, out var item);
    public static bool Remove<T>(this List<T> source, Predicate<T> predicate, out T item)
    {
        var index = source.FindIndex(predicate);
        if(index >= 0)
        {
            item = source[index];
            source.RemoveAt(index);
            return true;
        }
        item = default;
        return false;
    }
    public static ObservableList<T> ToObservableList<T>(this IEnumerable<T> source)
    {
        return new ObservableList<T>(source);
    }

    public static T[] Clear<T>(this T[] array, T defaultValue = default)
    {
        for (int i = 0; i < array.Length; i++)
            array[i] = defaultValue;
        return array;
    }
}
