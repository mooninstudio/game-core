﻿using UnityEngine;

public static class MonoBehaviourExtensions
{
    public static bool TryGetComponent<T>(this Component component, out T result) where T : Component
    {
        result = component.GetComponent<T>();
        return result;
    }

    public static T EnsureComponent<T>(this Component component) where T : Component
    {
        return component.TryGetComponent<T>(out var comp) ? comp : component.gameObject.AddComponent<T>();
    }
}