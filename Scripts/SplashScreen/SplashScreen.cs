﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
    [SerializeField] private Image logo;
    [SerializeField] private float fadeInDuration;
    [SerializeField] private float fadeOutDuration;
    [SerializeField] private float stayDuration;
    [SerializeField] private int goToScene;

    private void Start()
    {
        StartCoroutine(SplashScreenCoroutine(() => SceneManager.LoadScene(goToScene)));
    }
    private IEnumerator SplashScreenCoroutine(Action onComplete)
    {
        yield return ProcessCoroutine(fadeInDuration, t => FadeLogo(t));
        yield return ProcessCoroutine(stayDuration, t => { });
        yield return ProcessCoroutine(fadeOutDuration, t => FadeLogo(1 - t));
        onComplete();
    }
    private IEnumerator ProcessCoroutine(float duration, Action<float> onProcess)
    {
        var t = 0f;
        while (t != 1)
        {
            t = Mathf.MoveTowards(t, 1f, Time.deltaTime / duration);
            onProcess(t);
            yield return null;
        }
    }
    private void FadeLogo(float alpha)
    {
        var color = new Color(logo.color.r, logo.color.g, logo.color.b, alpha);
        logo.color = color;
    }
}
