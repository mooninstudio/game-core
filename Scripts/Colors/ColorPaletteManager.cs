using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class ThemeManager
{
    public static Theme[] Themes => _themes ?? (_themes = Resources.LoadAll<Theme>("Themes"));

    private static Theme[] _themes;
    private static int activeThemeId;

    public static void SetOrDefault(int themeId)
    {
        Set(Themes.Length > themeId ? themeId : 0);
    }
    public static void Set(int paletteId)
    {
        var theme = Themes[paletteId];
        var users = GetUsersOnScene();
        users.ForEach(user => user.OnApply(theme.GetColor(user.Color)));
        activeThemeId = paletteId;
    }

    private static IEnumerable<ThemeUser> GetUsersOnScene()
    {
        return Resources.FindObjectsOfTypeAll<ThemeUser>();
    }

    public static bool TryGetColor(ThemeColor themeColor, out Color color)
    {
        var theme = Themes[activeThemeId];

        if (theme == null)
        {
            color = Color.clear;
            return false;
        }
        else
        {
            color = theme.GetColor(themeColor);
            return true;
        }
    }
}