using UnityEngine;

public abstract class ThemeUser : MonoBehaviour
{
    [SerializeField] protected ThemeColor colorFromPalette;
    [SerializeField] protected bool ignoreAlpha;

    public ThemeColor Color => colorFromPalette;

    public abstract void OnApply(Color color);
    protected Color GetColorAlphaStay(Color current, Color newColor)
    {
        return new Color(newColor.r, newColor.g, newColor.b, current.a);
    }

    private void OnValidate() => UpdateColor();

    private void Awake() {
        UpdateColor();
    }

    public virtual void ChangeColorFromPalette(ThemeColor color)
    {
        colorFromPalette = color;
        UpdateColor();
    }

    public virtual void UpdateColor()
    {
        if (ThemeManager.TryGetColor(colorFromPalette, out var color))
            OnApply(color);
        else
            Debug.LogWarning("Couldn't access palette. Try reapplying the palette.");
    }
}