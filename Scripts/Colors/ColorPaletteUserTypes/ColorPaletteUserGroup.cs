using System.Linq;
using UnityEngine;

public class ColorPaletteUserGroup : ThemeUser
{
    [SerializeField] private ThemeUser[] group = new ThemeUser[0];

    public override void OnApply(Color color) {}
    public override void UpdateColor()
    {
        if(!group.IsNullOrEmpty())
            group.Where(x => x != null).ForEach(x => x.ChangeColorFromPalette(colorFromPalette));
    }
    public override void ChangeColorFromPalette(ThemeColor color)
    {
        colorFromPalette = color;
        group.ForEach(x => x.ChangeColorFromPalette(colorFromPalette));
    }
}