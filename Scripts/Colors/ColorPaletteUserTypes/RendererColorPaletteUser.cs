using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class RendererColorPaletteUser : ThemeUser
{
    public override void OnApply(Color color)
    {
        var renderer = GetComponent<Renderer>();
        renderer.sharedMaterial.color = ignoreAlpha ? GetColorAlphaStay(renderer.sharedMaterial.color, color) : color;
    }
}