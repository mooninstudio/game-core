﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteRendererColorPaletteUser : ThemeUser
{
    public override void OnApply(Color color)
    {
        var target = GetComponent<SpriteRenderer>();
        target.color = ignoreAlpha ? GetColorAlphaStay(target.color, color) : color;
    }
}
