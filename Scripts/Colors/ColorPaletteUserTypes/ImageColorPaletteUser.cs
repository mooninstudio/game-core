using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageColorPaletteUser : ThemeUser
{
    public override void OnApply(Color color)
    {
        var image = GetComponent<Image>();
        image.color = ignoreAlpha ? GetColorAlphaStay(image.color, color) : color;
    }
}