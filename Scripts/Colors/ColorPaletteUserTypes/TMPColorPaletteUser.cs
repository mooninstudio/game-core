using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TMPColorPaletteUser : ThemeUser
{

    public override void OnApply(Color color)
    {
        var target = GetComponent<TextMeshProUGUI>();
        target.color = ignoreAlpha ? GetColorAlphaStay(target.color, color) : color;
    }
}