using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BackgroundColorPaletteUser : ThemeUser
{
    public override void OnApply(Color color)
    {
        var target = GetComponent<Camera>();
        target.backgroundColor = ignoreAlpha ? GetColorAlphaStay(target.backgroundColor, color) : color ;
    }
}