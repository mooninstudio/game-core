using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Refs/New ColorPaletteStore", fileName="ColorPaletteStore")]
public class ColorPaletteStore : ScriptableObject
{
    [SerializeField] private string themesPath;
    [SerializeField, HideInInspector] private Theme savedPalette;

    private Theme[] palettes;
    public Theme[] Palettes {
        get => palettes ?? (palettes = LoadAll());
    }

    public void SavePalette(Theme palette)
    {
        savedPalette = palette;
    }

    public Theme LoadPalette()
    {
        return savedPalette;
    }

    private Theme[] LoadAll()
    {
        return Resources.LoadAll<Theme>(themesPath);
    }

    private Theme ByIdOrDefault(int index)
    {
        return Palettes.Length > index ? Palettes[index] : Palettes.First();
    }
}