using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName="Custom/Colors/Theme")]
public class Theme : ScriptableObject
{
    //[Button("Apply")] private void ApplyBtn_OnClick() => ThemeManager.Apply(this, debug);
    [Header("Mode")]
    [SerializeField] private bool debug = false;
    [SerializeField] private bool preview = true;
    [Space]
    [SerializeField] private Color background;
    [SerializeField] private Color uiActive;
    [SerializeField] private Color uiDisabled;
    [SerializeField] private Color player;
    [Header("Dot colors")]
    [SerializeField] private Color dotGrid;
    [SerializeField] private Color dotBasic;

    private bool isCurrent;

    public Color GetColor(ThemeColor themeColor)
    {
        switch (themeColor)
        {
            case ThemeColor.Background: return background;
            case ThemeColor.UI_Active: return uiActive;
            case ThemeColor.UI_Disabled: return uiDisabled;
            case ThemeColor.Player: return player;
            case ThemeColor.Dot_Grid: return dotGrid;
            case ThemeColor.Dot_Basic: return dotBasic;
            default: throw new System.Exception($"Unknown {nameof(ThemeColor)}: {themeColor}");
        }
    }

    // private void OnValidate()
    // {
    //     if(preview && isCurrent)
    //         ThemeManager.Apply(this, debug);
    // }

    public void SetCurrent(bool isCurrent) => this.isCurrent = isCurrent;
}

public enum ThemeColor
{
    None = -1,
    Background = 0,
    UI_Active = 1,
    UI_Disabled = 2,
    Player = 3,
    //--Dots--
    Dot_Grid = 10,
    Dot_Basic = 11,
}

public static class ColorFromPaletteHelpers
{
    public static string GetName(this ThemeColor color)
    {
        switch (color)
        {
            case ThemeColor.Background: return "Background";
            case ThemeColor.Player: return "Player";
            case ThemeColor.UI_Active: return "UI Active";
            case ThemeColor.UI_Disabled: return "UI Disabled";
            case ThemeColor.Dot_Grid: return "Grid";
            case ThemeColor.Dot_Basic: return "Dots";
            default: throw new System.Exception($"Unknown color {color}");
        }
    }
}