using System.Collections.Generic;
using UnityEngine;
using System;

public static class Refs
{
	private const string REFS_PATH = "Refs";

	private static Dictionary<string, object> refsCache = new Dictionary<string, object>();

	public static T Get<T>() where T : ScriptableObject
	{
		var refsFileName = typeof(T).FullName;

		if (TryLoadFromCache(refsFileName, out T refs))
			return refs;

		refs = Resources.Load<T>($"{REFS_PATH}/{refsFileName}");
		if (refs == null)
			throw new Exception($"No refs file found: {refsFileName}");
		refsCache.Add(refsFileName, refs);
		return refs;
	}
	private static bool TryLoadFromCache<T>(string key, out T refs)
	{
		refs = default;

		if (!refsCache.TryGetValue(key, out var obj))
			return false;

		refs = (T)obj;
		return refs != null;
	}
	public static void With<T>(Action<T> action) where T : ScriptableObject
	{
		action(Get<T>());
	}
}