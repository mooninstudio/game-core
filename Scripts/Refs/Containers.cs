using System.Collections.Generic;
using UnityEngine;

public static class Containers
{
    private static Transform root = new GameObject("Containers").transform;
    private static Dictionary<string, Transform> containers = new Dictionary<string, Transform>();

    public static Transform GetOrCreate(string name)
    {
        if (containers.ContainsKey(name))
            return containers[name];
        
        var container = new GameObject(name).transform;
        container.SetParent(root);
        containers.Add(name, container);
        return container;
    }
}