using UnityEngine;

public class ResourceRef<T>
    where T : ScriptableObject
{
    private string path;
    private T resource;
    public T Resource => resource ?? (resource = Resources.Load<T>($"{path}/{typeof(T)}"));

    public ResourceRef(string path = "")
    {
        this.path = path;
    }
}