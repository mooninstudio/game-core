using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Moonin.SimpleAnimator
{
    public class SimpleAnimator
    {
        private GameObject target;
        private List<Animation> playingAnimations = new List<Animation>();

        public SimpleAnimator(GameObject target)
        {
            this.target = target;
            PlayCoroutine().Run();
        }

        public IAnimation Play(AnimationClip clip, bool loop = false, int layer = 0)
        {
            var animation = new Animation(clip, loop, layer);
            var insertAt = playingAnimations.Any() ? playingAnimations.FindLastIndex(anim => anim.Layer <= layer) + 1 : 0;
            playingAnimations.Insert(insertAt, animation);
            return animation;
        }

        private IEnumerator PlayCoroutine()
        {
            while (true)
            {
                playingAnimations.ForEach(anim => 
                {
                    anim.Progress(Time.deltaTime);
                    anim.Clip.SampleAnimation(target, anim.Time);
                    if(anim.Completed && anim.Loop)
                        anim.Rewind();
                });
                playingAnimations = playingAnimations.Where(anim => !anim.Stopped && !anim.Completed).ToList();
                yield return null;
            }
        }
    }

    public class Animation : IAnimation
    {
        public AnimationClip Clip { get; private set; }
        public float Length { get; private set; }
        public float Time { get; private set; }
        public bool Loop { get; private set; }
        public int Layer { get; private set; }
        public bool Completed => Time == Length;
        public bool Stopped { get;  private set; }

        public Animation(AnimationClip clip, bool loop, int layer)
        {
            Clip = clip;
            Loop = loop;
            Length = clip.length.Min(0.01f);
            Time = 0;
            Layer = layer;
        }
        public void Progress(float timeDelta) => Time =  Mathf.MoveTowards(Time, Length, timeDelta);
        public void Rewind() => Time = 0;
        public void Stop() => Stopped = true;
    }
    public interface IAnimation
    {
        void Stop();
    }
}