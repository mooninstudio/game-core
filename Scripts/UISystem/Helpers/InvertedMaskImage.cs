using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Moonin.UISystem
{
    public class InvertedMaskImage : Image {
        public override Material materialForRendering
        {
            get
            {
                Material result = Instantiate(base.materialForRendering);
                result.SetInt("_StencilComp", (int) CompareFunction.NotEqual);
                return result;
            }
        }
}
}