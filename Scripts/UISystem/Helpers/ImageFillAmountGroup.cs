using UnityEngine;
using UnityEngine.UI;

namespace Moonin.UISystem
{
    [ExecuteInEditMode]
    public class ImageFillAmountGroup : MonoBehaviour
    {
        [SerializeField] private Image[] group = new Image[0];
        [SerializeField, Range(0, 1)] private float fillAmount;
        
        private void OnValidate()
        {
            group.ForEach(x => x.fillAmount = fillAmount);    
        }

    }
}