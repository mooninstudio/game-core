using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Moonin.UISystem
{
    [RequireComponent(typeof(RectTransform))]
    public class ScrollMenuDriver : MonoBehaviour
    {
        [SerializeField] private float _spacing = 20;
        [SerializeField] private float _freeScrollMultiplier = 100;
        [SerializeField] private float _freeScrollDumping = 2;
        [SerializeField] private float _minRecenterSpeed = 200;
        [SerializeField] private float _recenterSpeedByDistMultiplier = 200;
        [SerializeField] private float _springBackDistance = 200;

        public IObservable<ItemDataLoad> LoadDataStream => _loadDataSubject.AsObservable();

        private readonly Subject<ItemDataLoad> _loadDataSubject = new();
        private ConfigurableOverlay _overlay;
        private RectTransform _rectTransform;
        private List<ScrollItem> _items;
        private readonly Buffer<float> _freeScrollSamples = new(10);
        private Coroutine _scrolling;
        private float _itemWidth;
        private float _lastDrag;
        private float _freeScrollMinStep;
        private float _width;
        private int _dataCount;
        private int _itemsCount;
        private int _itemsCountHalf;
        private int _leftItemIndex;
        private int _midItemId;
        private int _rightItemIndex;
        private bool _dragging;

        private const UIEventType InteractMask = UIEventType.Drag | UIEventType.PointerDown | UIEventType.PointerUp;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _width = _rectTransform.rect.width;
        }

        private void InitializeOverlay()
        {
            var overlayObject = new GameObject("Drag overlay",
                typeof(Image),
                typeof(RectTransform));

            var overlayRect = overlayObject.GetComponent<RectTransform>();
            overlayRect.SetAndStretchToParentSize(_rectTransform);

            var overlayImage = overlayObject.GetComponent<Image>();
            overlayImage.color = Color.clear;

            _overlay = overlayObject.AddComponent<ConfigurableOverlay>();
            _overlay.Configure(InteractMask);
            _overlay.PointerDragStream.Subscribe(OnDrag);
            _overlay.PointerDownStream.Subscribe(_ => OnStartDrag());
            _overlay.PointerUpStream.Subscribe(_ => OnEndDrag());
        }

        public List<TItemElement> Setup<TItemElement>(Func<TItemElement> itemElementFactory, int dataCount)
            where TItemElement : IUIElement
        {
            var itemElements = new List<TItemElement>();
            _dataCount = dataCount;

            var itemElement = itemElementFactory();
            itemElement.RectTransform.SetParent(transform);
            itemElements.Add(itemElement);

            _itemWidth = itemElement.RectTransform.rect.width;
            _freeScrollMinStep = (_itemWidth + _spacing) / _width;
            _itemsCount = Mathf.Min(CalculateItemsCount(_itemWidth), dataCount);
            _itemsCountHalf = Mathf.FloorToInt(_itemsCount * .5f);

            for (var i = 1; i < _itemsCount; i++)
            {
                itemElement = itemElementFactory();
                itemElement.RectTransform.SetParent(transform);
                itemElements.Add(itemElement);
            }

            _items = itemElements.Select((x, i) => new ScrollItem(x.RectTransform, i)).ToList();

            PositionItems(0);
            InitializeOverlay();

            return itemElements;
        }

        public void FocusData(int dataId)
        {
            var preDataCount = dataId;
            var postDataCount = _dataCount - dataId - 1;

            if (preDataCount < _itemsCountHalf)
                PositionItems(preDataCount - _itemsCountHalf);
            else if (postDataCount < _itemsCountHalf)
                PositionItems(_itemsCountHalf - postDataCount);
            else
                PositionItems(0);

            var leftDataId = Mathf.Clamp(dataId - _itemsCountHalf, 0, _dataCount - _itemsCount);
            for (var i = 0; i < _itemsCount; i++)
                _loadDataSubject.OnNext(new ItemDataLoad(i, leftDataId + i));
        }
        
        private int CalculateItemsCount(float itemWidth)
        {
            var fitting = Mathf.CeilToInt(_width / (itemWidth + _spacing));
            fitting = fitting % 2 == 0 ? fitting + 1 : fitting;
            return fitting + 2;
        }

        private void OnStartDrag()
        {
            _scrolling?.Stop();
            _dragging = true;
        }

        private void OnDrag(PointerEventData data)
        {
            var drag = (_rectTransform.ScreenPointToLocalNormalised(data.position) -
                        _rectTransform.ScreenPointToLocalNormalised(data.pressPosition)).x;
            var delta = drag - _lastDrag;
            _freeScrollSamples.Push(delta * Time.deltaTime);
            Scroll(delta);
            _lastDrag = drag;
        }

        private void Scroll(float value)
        {
            var step = value * _width;
            step = SpringBack(step);

            MoveItems(step);
            HandleEdgePasses(step.Sign());
            _midItemId = FindMidItemId();
        }

        private void MoveItems(float step)
        {
            _items.ForEach(x => x.RectTransform.anchoredPosition += Vector2.right * step);
        }

        private int FindMidItemId()
        {
            var best = 0;
            for (var i = 1; i < _itemsCount; i++)
                best = _items[best].RectTransform.anchoredPosition.x.Abs() >
                       _items[i].RectTransform.anchoredPosition.x.Abs()
                    ? i
                    : best;
            return best;
        }

        private void HandleEdgePasses(float dir)
        {
            switch (dir)
            {
                case < 0 when PassedRightEdge() && _items[_rightItemIndex].DataIndex < _dataCount - 1:
                    SwapItemsToRight();
                    break;
                case > 0 when PassedLeftEdge() && _items[_leftItemIndex].DataIndex > 0:
                    SwapItemsToLeft();
                    break;
            }
        }

        private bool PassedLeftEdge()
        {
            var leftItem = _items[_leftItemIndex];
            var leftItemNormalizedX =
                _rectTransform.rect.PointToNormalizedUnclamped(leftItem.RectTransform.localPosition).x;
            return leftItemNormalizedX > 0;
        }

        private bool PassedRightEdge()
        {
            var rightItem = _items[_rightItemIndex];
            var rightItemNormalizedX =
                _rectTransform.rect.PointToNormalizedUnclamped(rightItem.RectTransform.localPosition).x;
            return rightItemNormalizedX < 1;
        }

        private float SpringBack(float scrollStep)
        {
            var dir = scrollStep.Sign();
            if (dir > 0)
            {
                scrollStep = Mathf.Min(scrollStep.Abs(),
                    _items[_leftItemIndex].RectTransform.anchoredPosition.x.Abs() + _springBackDistance);
                if (_midItemId == _leftItemIndex && !_dragging)
                    ForceRecenter();
            }
            else
            {
                scrollStep = Mathf.Min(scrollStep.Abs(),
                    _items[_rightItemIndex].RectTransform.anchoredPosition.x.Abs() + _springBackDistance);
                if (_midItemId == _rightItemIndex && !_dragging)
                    ForceRecenter();
            }

            return dir * scrollStep;
        }

        private void ForceRecenter()
        {
            _scrolling?.Stop();
            _scrolling = Recenter().Run();
        }

        private void SwapItemsToRight(Action<int> onMovedId = null)
        {
            var leftItem = _items[_leftItemIndex];
            var rightItem = _items[_rightItemIndex];

            leftItem.RectTransform.anchoredPosition =
                rightItem.RectTransform.anchoredPosition + Vector2.right * (_spacing + _itemWidth);

            var newLeftItemDataIndex = rightItem.DataIndex + 1;
            _loadDataSubject.OnNext(new ItemDataLoad(_leftItemIndex, newLeftItemDataIndex));
            leftItem.SetDataIndex(newLeftItemDataIndex);

            _rightItemIndex = _leftItemIndex;
            _leftItemIndex = ++_leftItemIndex % _itemsCount;
            onMovedId?.Invoke(_rightItemIndex);
            HandleEdgePasses(-1);
        }

        private void SwapItemsToLeft(Action<int> onMovedId = null)
        {
            var leftItem = _items[_leftItemIndex];
            var rightItem = _items[_rightItemIndex];

            rightItem.RectTransform.anchoredPosition =
                leftItem.RectTransform.anchoredPosition - Vector2.right * (_spacing + _itemWidth);

            var newRightItemDataIndex = leftItem.DataIndex - 1; 
            _loadDataSubject.OnNext(new ItemDataLoad(_rightItemIndex, newRightItemDataIndex));
            rightItem.SetDataIndex(newRightItemDataIndex);

            _leftItemIndex = _rightItemIndex;
            _rightItemIndex = _rightItemIndex == 0 ? _itemsCount - 1 : _rightItemIndex - 1;
            onMovedId?.Invoke(_leftItemIndex);
            HandleEdgePasses(1);
        }

        private void OnEndDrag()
        {
            var freeScrollStep = _freeScrollSamples.ToArray().Average() * _freeScrollMultiplier;

            _dragging = false;
            _lastDrag = 0;
            _freeScrollSamples.Clear();

            _scrolling?.Stop();
            _scrolling = FreeScroll(freeScrollStep).Run();
        }

        private IEnumerator FreeScroll(float freeScrollStep)
        {
            var dir = freeScrollStep.Sign();
            freeScrollStep = freeScrollStep.Abs();

            while (freeScrollStep > _freeScrollMinStep)
            {
                Scroll(freeScrollStep * dir);
                freeScrollStep = Mathf.MoveTowards(freeScrollStep, 0, _freeScrollDumping * Time.deltaTime);
                yield return null;
            }

            yield return Recenter();
        }

        private IEnumerator Recenter()
        {
            var midItem = _items[_midItemId];

            while (midItem.RectTransform.anchoredPosition.x != 0)
            {
                var dir = -midItem.RectTransform.anchoredPosition.x.Sign();
                var distToCenter = midItem.RectTransform.anchoredPosition.x.Abs();
                var distToCenterNormalized = distToCenter / _width;
                var speed = (_minRecenterSpeed + _recenterSpeedByDistMultiplier * distToCenterNormalized);

                var step = Mathf.Min(distToCenter, speed * Time.deltaTime);

                MoveItems(step * dir);
                HandleEdgePasses(step.Sign());
                yield return null;
            }
        }

        private void PositionItems(int offset)
        {
            offset = Mathf.Clamp(offset, -_itemsCountHalf, _itemsCountHalf);
            var step = new Vector2(_itemWidth + _spacing, 0);
            var pos = -(_itemsCountHalf + offset) * step;

            for (var i = 0; i < _itemsCount; i++)
            {
                _items[i].RectTransform.anchoredPosition = pos;
                pos += step;
            }

            _leftItemIndex = 0;
            _rightItemIndex = _itemsCount - 1;
        }
        
        private class ScrollItem
        {
            public RectTransform RectTransform { get; }
            public int DataIndex { get; private set; }

            public ScrollItem(RectTransform rectTransform, int dataIndex)
            {
                RectTransform = rectTransform;
                DataIndex = dataIndex;
            }

            public void SetDataIndex(int dataId)
            {
                DataIndex = dataId;
            }
        }

        public struct ItemDataLoad
        {
            public int ItemIndex { get; }

            public int DataIndex { get; }

            public ItemDataLoad(int itemIndex, int dataIndex)
            {
                ItemIndex = itemIndex;
                DataIndex = dataIndex;
            }
        }
    }
}