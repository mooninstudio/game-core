using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
    public abstract class UIScreenBase : MonoBehaviour, IUIScreen
    {
        [Button("Switch to this")]
        protected void E_SwitchToThis()
        {
            var siblingScreens = transform.parent.GetComponentsInCloseChildren<UIScreenBase>(false);
            siblingScreens.ForEach(s => s.SetActive(false));
            SetActive(true);
        }

        public IObservable<Unit> BlockStream => blockSubject.Where(x => x).AsUnitObservable();
        public IObservable<Unit> UnblockStream => blockSubject.Where(x => !x).AsUnitObservable();
        public bool IsActive => gameObject.activeSelf;

        private HashSet<Widget> activeWidgets = new HashSet<Widget>();
        private Subject<bool> blockSubject = new Subject<bool>();
        private List<Widget> popOnDismountWidgets = new List<Widget>();
        private int blockers;

        public virtual void Setup()
        {
            SetActive(false);
        }
        public virtual void OnAfterSetup() { }
        protected void SetActive(bool active) => gameObject.SetActive(active);

        public IObservable<Unit> Mount() => Observable.Concat(
            OnBeforeMount(),
            MountWidgets(),
            OnAfterMount()
        ).Last().DoOnSubscribe(() => SetActive(true));
        protected virtual IObservable<Unit> OnBeforeMount() => Observable.ReturnUnit();
        protected virtual IObservable<Unit> OnAfterMount() => Observable.ReturnUnit();
        private IObservable<Unit> MountWidgets()
        {
            var widgets = activeWidgets.ToArray();
            return widgets.Any() ? widgets.Select(w => w.Show()).Merge().Last() : Observable.ReturnUnit();
        } 

        public IObservable<Unit> Dismount() => Observable.Concat(
            OnBeforeDismount(),
            DismountWidgets(),
            OnAfterDismount()
        ).Last().DoOnCompleted(() => { SetActive(false); AutoPop(); });
        protected virtual IObservable<Unit> OnBeforeDismount() => Observable.ReturnUnit();
        protected virtual IObservable<Unit> OnAfterDismount() => Observable.ReturnUnit();
        private IObservable<Unit> DismountWidgets()
        {
            var widgets = activeWidgets.ToArray();
            return widgets.Any() ? widgets.Select(w => w.Hide()).Merge().Last() : Observable.ReturnUnit();
        } 
        private void AutoPop()
        {
            Pop(popOnDismountWidgets.ToArray());
            popOnDismountWidgets.Clear();
        }

        protected IObservable<Unit> Push(params Widget[] widgets)
        {
            var pushedWidgets = widgets.Where(activeWidgets.Add).ToArray();
            return pushedWidgets.Any() && IsActive ? pushedWidgets.Select(w => w.Show()).Merge().Last() : Observable.ReturnUnit();
        }
        protected IObservable<Unit> PushAutoPop(params Widget[] widgets)
        {
            var widgetsToPush = widgets.Except(activeWidgets).ToArray();
            popOnDismountWidgets.AddRange(widgetsToPush);
            return Push(widgetsToPush.ToArray());
        }
        protected IObservable<Unit> Pop(params Widget[] widgets)
        {
            var poppedWidgets = widgets.Where(activeWidgets.Remove).ToArray();
            return poppedWidgets.Any() && IsActive ? poppedWidgets.Select(w => w.Hide()).Merge().Last() : Observable.ReturnUnit();
        }
        protected IObservable<Unit> PopAll()
        {
            return activeWidgets.Any() && IsActive ? activeWidgets.Select(w => w.Hide()).Merge().Last() : Observable.ReturnUnit();
        }

        protected IObservable<T> AsBlocking<T>(IObservable<T> action)
        {
            return action
                .DoOnSubscribe(Block)
                .DoOnCompleted(Unblock);
        }
        protected void InvokeAsBlocking(IObservable<Unit> action)
        {
            AsBlocking(action).Subscribe();
        }
        protected void Block()
        {
            blockers++;
            blockSubject.OnNext(true);
        }
        protected void Unblock()
        {
            blockers = Mathf.Max(blockers - 1, 0);
            blockSubject.OnNext(blockers != 0);
        }
    }

    public interface IUIScreen
    {
        void Setup();
        void OnAfterSetup();
        IObservable<Unit> Mount();
        IObservable<Unit> Dismount();
    }
}