using System;
using System.Collections.Generic;
using System.Linq;
using Moonin.State;
using UniRx;
using UnityEngine;
using static Moonin.State.BaseState;

namespace Moonin.UISystem
{   
    public abstract class UIBase<TState, TScreen> : MonoBehaviour
        where TState : BaseState
        where TScreen : UIScreenBase<TState>
    {
        [SerializeField] private Widget blockingPanel;

        private HashSet<TScreen> screens = new HashSet<TScreen>();
        protected Property<TScreen> mountedScreen = new Property<TScreen>();

        private Dictionary<TScreen, HashSet<TScreen>> screenRelations = new Dictionary<TScreen, HashSet<TScreen>>();
        private int blockers;

        public void Setup(TState state)
        {
            RegisterScreens();
            SetupScreens(state);
            //SubscribeBlocker();
            OnAfterSetup(state);
        }
        private void SetupScreens(TState state)
        {
            screens.ForEach(s => s.Setup(state));
        }
        private void SubscribeBlocker()
        {
            screens.Select(x => x.BlockStream).Merge().Subscribe(_ => Block());
            screens.Select(x => x.UnblockStream).Merge().Subscribe(_ => Unblock());
        }
        protected abstract void RegisterScreens();

        protected virtual void OnAfterSetup(TState state)
        {
            screens.ForEach(s => s.OnAfterSetup());
        }

        protected void RegisterScreen(TScreen screen) => screens.Add(screen);
        protected void RegisterRelatedScreen(TScreen screen, params TScreen[] relatedScreens)
        {
            RegisterScreen(screen);
            AddScreenRelations(screen, relatedScreens);
        }
        private void AddScreenRelations(TScreen screen, params TScreen[] relatedScreens)
        {
            foreach (var s in relatedScreens)
            {
                if(screenRelations.ContainsKey(s))
                    screenRelations[s].Add(screen);
                else
                    screenRelations.Add(s, new HashSet<TScreen>() { screen });
            }
        }

        protected virtual IObservable<Unit> MountScreen(TScreen screen)
        {
            if(IsMounted(screen))
                return Observable.ReturnUnit();

            IEnumerable<TScreen> sidesToHide = mountedScreen.HasValue && screenRelations.ContainsKey(mountedScreen.Value) ? screenRelations[mountedScreen.Value] : new HashSet<TScreen>();
            IEnumerable<TScreen> sidesToShow = screenRelations.ContainsKey(screen) ? screenRelations[screen] : new HashSet<TScreen>();

            var toDismount = sidesToHide.Except(sidesToShow);
            var toMount = sidesToShow.Except(sidesToHide);

            return AsBlocking(Observable.Concat(
                //  hiding
                DismountCurrent().Merge(toDismount.Any() ? toDismount.Select(x => x.Dismount()).Merge() : Observable.ReturnUnit()),
                //  showing
                toMount.Append(screen).Select(x => x.Mount()).Merge()
            ).Last().DoOnCompleted(() => mountedScreen.Set(screen)));
        }
        protected bool IsMounted(TScreen screen) => mountedScreen.Value == screen;
        protected IObservable<Unit> DismountCurrent()
        {
            return mountedScreen.Value != null ? 
                AsBlocking(mountedScreen.Value.Dismount().DoOnCompleted(() => mountedScreen.Set(null)))
                : Observable.ReturnUnit();
        }

        protected IObservable<T> AsBlocking<T>(IObservable<T> action)
        {
            return action
                .DoOnSubscribe(Block)
                .DoOnCompleted(Unblock);
        }
        private void Block()
        {
            blockers++;
            blockingPanel?.Activate();
        }
        private void Unblock()
        {
            blockers = Mathf.Max(blockers - 1, 0);
            if(blockers == 0)
                blockingPanel?.Deactivate();
        }
    }
}