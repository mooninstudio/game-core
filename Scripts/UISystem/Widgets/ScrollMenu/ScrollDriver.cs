using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Collections;
using System.Linq;
using System;

namespace Moonin.UISystem
{   
    [System.Serializable]
    public class ScrollDriverSettings
    {
        [Header("Positioning")]
        public Vector2 scrollDir = Vector2.right;
        public float itemWidth = 100;
        public float spacing = 20;
        public Vector2 centerPoint = Vector2.one * .5f;
        [Header("Elasticity")]
        public float elasticity = .3f;
        [Header("Inertion")]
        public float maxSpeed = 6f;
        public float minSpeed = .3f;
        public float breaking = 3f;
        [Header("Recenter")]
        public float recenterSpeed = 6f;

    }

    public class ScrollDriver<TId>
    {
        public IObservable<Guid> ItemFocuseStream => itemFocuseSubject.AsObservable().DistinctUntilChanged();
        private Subject<Guid> itemFocuseSubject = new Subject<Guid>();

        public IObservable<SwapData<TId>> SwapStream => swapSubject.AsObservable();
        private Subject<SwapData<TId>> swapSubject = new Subject<SwapData<TId>>();

        private ScrollDriverSettings settings;
        private Vector2 _scrollDir;
        private float _itemsSpacing;
        private float _screenSizeInScrollDir;
        private float _minTotalScroll;
        private float _maxTotalScroll;
        private float _elasticMinTotalScroll;
        private float _elasticMaxTotalScroll;
        private int _itemsCountHalf;

        private List<IScrollItem<TId>> items;
        private DragOverlay scrollOverlay;
        private Coroutine inertion;
        private Coroutine recentering;
        private float scrollSpeedDir;
        private float scrollSpeed;
        private float totalScroll;
        private int currentStep;
        private int collectionCount;

        public ScrollDriver(ScrollDriverSettings settings, IObservable<Vector2> dragStream, IObservable<Unit> dragEndStream)
        {
            this.settings = settings;
            dragStream.Subscribe(HandleDrag);
            dragEndStream.Subscribe(HandleDragStop);
            
            _scrollDir = settings.scrollDir.normalized;
            _screenSizeInScrollDir = _scrollDir.x * Screen.width + _scrollDir.y * Screen.height;
            _itemsSpacing = (settings.itemWidth + settings.spacing) / _screenSizeInScrollDir;
        }
        private void HandleDrag(Vector2 drag)
        {
            inertion?.Stop();
            recentering?.Stop();

            var scroll = DragToScroll(drag);
            if(ClampScroll(scroll, out scroll))
                scrollSpeed = 0;
            else
            {
                var speed = scroll / Time.deltaTime;
                scrollSpeed = speed.Abs().Max(settings.maxSpeed);
                scrollSpeedDir = speed.Sign();
            }

            Scroll(scroll);
        } 
        private float DragToScroll(Vector2 drag)
        {
            return Vector2.Dot(_scrollDir, drag) / _scrollDir.magnitude;
        }
        private bool ClampScroll(float scroll, out float limitedScroll)
        {
            limitedScroll = (totalScroll + scroll).Clamp(_elasticMinTotalScroll, _elasticMaxTotalScroll) - totalScroll;
            return limitedScroll.Round(2) != scroll.Round(2);
        }
        private void Scroll(float scroll)
        {
            var lastTotalScroll = totalScroll;
            totalScroll += scroll;

            SwapItems();
            MoveItems(scroll);
            itemFocuseSubject.OnNext(items[CalculateStep()].ItemId);
        }
        private void SwapItems()
        {
            var currentStep = this.currentStep;
            var newStep = CalculateStep();
            this.currentStep = newStep;

            var stepsDiff = newStep - currentStep;
            if(stepsDiff == 0)
                return;

            var backward = stepsDiff < 0;

            for (int i = 0; i < stepsDiff.Abs(); i++)
            {
                if(backward)
                {
                    if(CanSwapOnStep(--currentStep, backward))
                        SwapFrontToBack();
                }
                else
                {
                    if(CanSwapOnStep(++currentStep, backward))
                        SwapBackToFront();
                }
            }
        }
        private int CalculateStep()
        {
            return (-Mathf.RoundToInt(totalScroll / _itemsSpacing)).Clamp(0, collectionCount);
        }
        private bool CanSwapOnStep(int step, bool backward)
        {
            if(backward)
            {
                if(step < _itemsCountHalf || step >= collectionCount - _itemsCountHalf - 1)
                    return false;
            }
            else
            {
                if(step <= _itemsCountHalf || step > collectionCount - _itemsCountHalf - 1)
                    return false;
            }
            return true;
        }
        private void SwapBackToFront()
        {
            var swapped = items.First();
            items.RemoveAt(0);
            items.Add(swapped);
            swapped.Move(_scrollDir * (items.Count) * _itemsSpacing);
            swapSubject.OnNext(new SwapData<TId> { ItemId = swapped.ItemId, CurrentDataId = swapped.DataId, Forward = true });
        }
        private void SwapFrontToBack()
        {
            var swapped = items.Last();
            items.RemoveAt(items.Count - 1);
            items.Prepend(swapped);
            swapped.Move(-_scrollDir * (items.Count) * _itemsSpacing);
            swapSubject.OnNext(new SwapData<TId> { ItemId = swapped.ItemId, CurrentDataId = swapped.DataId, Forward = false });
        }
        private void MoveItems(float scroll)
        {
            items.ForEach(x => x.Move(_scrollDir * scroll));
        }

        private void HandleDragStop()
        {
            inertion?.Stop();
            recentering?.Stop();

            inertion = InertionCoroutine().Run();
        }
        private IEnumerator InertionCoroutine()
        {
            while(scrollSpeed > settings.minSpeed)
            {
                scrollSpeed = Mathf.MoveTowards(scrollSpeed, 0, settings.breaking * Time.deltaTime);
                var scroll = scrollSpeed * scrollSpeedDir * Time.deltaTime;
                if(ClampScroll(scroll, out scroll))
                    scrollSpeed = 0;
                Scroll(scroll);
                yield return null;
            }
            recentering = RecenterCoroutine(CalculateRecenterTotalScroll()).Run();
        }
        private IEnumerator RecenterCoroutine(float targetTotalScroll)
        {
            while (targetTotalScroll != totalScroll)
            {
                var frameTotalScroll = Mathf.Lerp(totalScroll, targetTotalScroll, settings.recenterSpeed * Time.deltaTime);
                var scroll = frameTotalScroll - totalScroll;
                Scroll(scroll);
                yield return null;
            }
        }

        private float CalculateRecenterTotalScroll()
        {
            return - CalculateStep() * _itemsSpacing;
        }
        
        public void LoadItems(List<IScrollItem<TId>> items, int collectionCount)
        {
            this.collectionCount = collectionCount;
            this.items = items;
            CalculateParameters();

            for (int i = 0; i < items.Count; i++)
                items[i].SetPosition(settings.centerPoint + _scrollDir * i * _itemsSpacing);
        }
        private void CalculateParameters()
        {
            _minTotalScroll = -(collectionCount - 1) * _itemsSpacing;
            _maxTotalScroll = 0;
            _elasticMinTotalScroll = _minTotalScroll - settings.elasticity;
            _elasticMaxTotalScroll = _maxTotalScroll + settings.elasticity;
            _itemsCountHalf = Mathf.FloorToInt(items.Count * .5f);
        }
    }

    public interface IScrollItem<T>
    {
        Guid ItemId { get; }
        T DataId { get; }

        void Move(Vector2 delta);
        void SetPosition(Vector2 position);
    }

    public struct SwapData<T>
    {
        public Guid ItemId { get; set; }
        public T CurrentDataId { get; set; }
        public bool Forward { get; set; }
    }
}