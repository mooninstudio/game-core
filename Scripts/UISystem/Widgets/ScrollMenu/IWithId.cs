namespace Moonin.UISystem
{   
    public interface IWithId<Tid>
    {
        Tid Id { get; }
    }
}