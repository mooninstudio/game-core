using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

namespace Moonin.UISystem
{   
    public class ScrollViewBase<TItem, TData, TId> : GroupWidget 
        where TItem : ScrollItemBase<TData, TId>
        where TData : IWithId<TId>
    {
        [SerializeField] protected ScrollDriverSettings scrollSettings;
        [SerializeField] private TItem itemPrefab;
        [SerializeField] private int itemsCount;

        public IObservable<TData> DataFocuseStream => scrollDriver.ItemFocuseStream.Select(GetDataByItemId);

        private DragOverlay dragOverlay;
        private ScrollDriver<TId> scrollDriver;
        private List<TId> charactersIds;
        private Dictionary<TId, TData> data;
        private Dictionary<Guid, TItem> items;

        protected override void OnAwake()
        {
            base.OnAwake();
            this.dragOverlay = GetComponent<DragOverlay>();
        }
        private void SwapItems(SwapData<TId> data)
        {
            var currentDataIndex = charactersIds.IndexOf(data.CurrentDataId);
            var newDataId = charactersIds[data.Forward ? currentDataIndex + itemsCount : currentDataIndex - itemsCount];
            var characterData = this.data[newDataId];
            items[data.ItemId].LoadData(characterData);
        }

        public void LoadData(List<TData> data)
        {
            ForceAwake();
            this.data = data.ToDictionary(c => c.Id);
            charactersIds = this.data.Keys.ToList();
            items = data.Take(itemsCount).Select(d =>
            {
                var item = Instantiate(itemPrefab, transform);
                item.ForceAwake();
                OnItemCreated(item);
                item.LoadData(d);
                return item;
            }).ToDictionary(i => i.ItemId);
            ResolveChildren();

            this.scrollDriver = new ScrollDriver<TId>(scrollSettings, dragOverlay.DragStream, dragOverlay.DragEndedStream);
            this.scrollDriver.SwapStream.Subscribe(SwapItems);
            scrollDriver.LoadItems(items.Values.Cast<IScrollItem<TId>>().ToList(), data.Count);
        }
        protected virtual void OnItemCreated(TItem item) { }
        protected TData GetDataByItemId(Guid itemId)
        {
            return data[items[itemId].DataId];
        }
    }
}