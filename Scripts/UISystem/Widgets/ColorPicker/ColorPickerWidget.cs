using System;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
    public class ColorPickerWidget : GroupWidget
    {
        [SerializeField] private PanelValuePickerWidget svPanel;
        [SerializeField] private SliderWidget hSlider;
        [SerializeField] private ImageWidget svPointer;
        [SerializeField] private ImageWidget hPointer;
        [SerializeField] private ImageWidget colorImg;
        [SerializeField] private ButtonWidget confirmBtn;
        [SerializeField] private ButtonWidget cancelBtn;

        public IObservable<Color> ColorChangedStream => colorChangedSubject.AsObservable();
        public IObservable<bool> ClosePickerStream => Observable.Merge(
            confirmBtn.PressedStream.Select(_ => true),
            cancelBtn.PressedStream.Select(_ => false)
        );

        private Subject<Color> colorChangedSubject = new Subject<Color>();
        private ColorHSV displayedColor;

        protected override void OnAwake()
        {
            base.OnAwake();
            svPanel.ValueStream.Subscribe(sv => SetSVPointerPosition(sv.x, sv.y));
            hSlider.ValueStream.Subscribe(SetHPointerPosition);

            hSlider.ValueStream.Subscribe(x => colorChangedSubject.OnNext(CombineColor(x)));
            svPanel.ValueStream.Subscribe(x => colorChangedSubject.OnNext(CombineColor(x)));
        }
        private Color CombineColor(float newH)
        {
            return Color.HSVToRGB(newH, displayedColor.S, displayedColor.V);
        }
        private Color CombineColor(Vector2 sv)
        {
            return Color.HSVToRGB(displayedColor.H, sv.x, sv.y);
        }

        public void SetColor(Color color)
        {
            Color.RGBToHSV(color, out var h, out var s, out var v);
            var plainColor = Color.HSVToRGB(h, 1, 1);

            SetSVPointerPosition(s, v);
            SetHPointerPosition(h);

            colorImg.SetColor(plainColor);

            displayedColor = new ColorHSV(color);
        }
        private void SetSVPointerPosition(float s, float v)
        {
            var svPointerPos = new Vector2(s, v);
            svPointer.RectTransform.anchorMin = svPointerPos;
            svPointer.RectTransform.anchorMax = svPointerPos;
        }
        private void SetHPointerPosition(float h)
        {
            var hPointerPos = new Vector2(h, .5f);
            hPointer.RectTransform.anchorMin = hPointerPos;
            hPointer.RectTransform.anchorMax = hPointerPos;
        }
    }
}