using System;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
    public class Widget : MonoBehaviour
    {
        //  TODO: Show & Hide should be able to interrupt each other / queue (1 next operation)
        public RectTransform RectTransform { get; private set; }
        public bool IsActive => gameObject.activeSelf;

        private bool awaken;
        
        public IObservable<Unit> Show() => OnBeforeShow().DoOnSubscribe(Activate);
        public IObservable<Unit> Hide() => OnBeforeHide().DoOnCompleted(Deactivate);
        public virtual void Activate() => gameObject.SetActive(true);
        public virtual void Deactivate() => gameObject.SetActive(false);
        public void ForceAwake() => OnAwake();

        protected virtual void OnAwake()
        {
            if(awaken)
                return;

            RectTransform = GetComponent<RectTransform>();
            Deactivate();
            awaken = true;
        }
        protected virtual IObservable<Unit> OnBeforeShow() => Observable.ReturnUnit();
        protected virtual IObservable<Unit> OnBeforeHide() => Observable.ReturnUnit();
        private void Awake() => OnAwake();
    }
}