using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
    public class GroupWidget : Widget
    {
        [SerializeField] private bool deep;

        private HashSet<Widget> activeWidgets = new HashSet<Widget>();
        private List<Widget> popOnDismountWidgets = new List<Widget>();
        protected List<Widget> children;

        protected override void OnAwake()
        {
            ResolveChildren();
            base.OnAwake();
        }
        protected void ResolveChildren()
        {
            children = deep ? transform.GetComponentsInChildren<Widget>(true).Where(x => x != this).ToList() : transform.GetComponentsInCloseChildren<Widget>(true);
            if(IsActive)
                Activate();
            else
                Deactivate();
        }

        public override void Activate()
        {
            base.Activate();
            children.ForEach(x => x.Activate());
        }
        public override void Deactivate()
        {
            base.Deactivate();
            children.ForEach(x => x.Deactivate());
        }
        
        protected override IObservable<Unit> OnBeforeShow() => children.Any() ? children.Select(child => child.Show()).Merge().Last() : Observable.ReturnUnit();
        protected override IObservable<Unit> OnBeforeHide() => children.Any() ? children.Select(child => child.Hide()).Merge().Last() : Observable.ReturnUnit();
    }
}