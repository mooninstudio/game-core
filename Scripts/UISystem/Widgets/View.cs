using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{
    public class View : Widget
    {
        private List<Widget> widgets = new List<Widget>();
        
        protected void AddWidget(Widget widget)
        {
            widgets.Add(widget);
        }
        
        protected override IObservable<Unit> OnBeforeShow()
        {
            return base.OnBeforeShow()
                .Concat(widgets
                    .Select(x => x.Show())
                    .Merge())
                .Last();
        }
        
        protected override IObservable<Unit> OnBeforeHide()
        {
            return base.OnBeforeShow()
                .Concat(widgets
                    .Select(x => x.Hide())
                    .Merge())
                .Last();
        }
    }
}
