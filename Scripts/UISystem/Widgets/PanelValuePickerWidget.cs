using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;

namespace Moonin.UISystem
{   
    public class PanelValuePickerWidget : GroupWidget
    {
        [SerializeField] private ConfigurableOverlay input;

        public IObservable<Vector2> ValueStream => Observable.Merge(
            input.PointerDragStream,
            input.PointerDownStream,
            input.PointerUpStream
        ).Select(ValueFromPointer);

        private readonly UIEventType eventMask = 
            UIEventType.Drag
            | UIEventType.PointerDown
            | UIEventType.PointerEnter
            | UIEventType.PointerExit
            | UIEventType.PointerUp;

        protected override void OnAwake()
        {
            input.Configure(eventMask);
            base.OnAwake();
        }

        private Vector2 ValueFromPointer(PointerEventData pointerData)
        {
            return input.RectTransform.ScreenPointToLocalNormalised(pointerData.position);
        }
    }
}