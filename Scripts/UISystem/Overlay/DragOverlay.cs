using UnityEngine;
using UniRx;
using UnityEngine.EventSystems;
using System;

namespace Moonin.UISystem
{   
    [RequireComponent(typeof(ConfigurableOverlay))]
    public class DragOverlay : MonoBehaviour
    {
        public IObservable<Vector2> DragStream => dargSubject.AsObservable();
        public IObservable<Vector2> TotalDragStream => totalDragSubject.AsObservable();
        public IObservable<Unit> DragEndedStream => dragEndedSubject.AsObservable();

        private Subject<Vector2> dargSubject = new Subject<Vector2>();
        private Subject<Vector2> totalDragSubject = new Subject<Vector2>();
        private Subject<Unit> dragEndedSubject = new Subject<Unit>();
        private ConfigurableOverlay overlay;
        private Vector2 totalDrag;

        private void Awake()
        {
            overlay = GetComponent<ConfigurableOverlay>();
            overlay.Configure(
                consumeMask: UIEventType.Drag,
                interceptMask: UIEventType.Drag | UIEventType.PointerUp
            );
            overlay.PointerDragStream.Subscribe(HandleDrag);
            overlay.PointerUpStream.Subscribe(HandlePointerUp);
        }
        private void HandleDrag(PointerEventData data)
        {
            var drag = new Vector2(data.delta.x / Screen.width, data.delta.y / Screen.height);
            totalDrag += drag;

            dargSubject.OnNext(drag);
            totalDragSubject.OnNext(totalDrag);
        }
        private void HandlePointerUp(PointerEventData data)
        {
            totalDrag = default;
            dragEndedSubject.OnNext();
        }
    }
}