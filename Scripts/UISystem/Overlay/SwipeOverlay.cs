using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;
using System;

namespace Moonin.UISystem
{   
    [RequireComponent(typeof(ConfigurableOverlay))]
    public class SwipeOverlay : MonoBehaviour
    {
        [SerializeField] private float maxTime = .5f;
        [SerializeField] private float minDistanceNormalised = .2f;

        public IObservable<Vector2> SwipeStream => swipeSubject.AsObservable();

        private Subject<Vector2> swipeSubject = new Subject<Vector2>();
        private ConfigurableOverlay overlay;
        private Vector2 downPoint;
        private float downTime;

        private void Awake()
        {
            overlay = GetComponent<ConfigurableOverlay>();
            overlay.Configure(
                UIEventType.PointerDown | UIEventType.PointerUp,
                UIEventType.PointerDown | UIEventType.PointerUp | UIEventType.PointerClick
            );
            overlay.PointerDownStream.Subscribe(HandlePointerDown);
            overlay.PointerUpStream.Subscribe(HandlePointerUp);
        }
        private void HandlePointerDown(PointerEventData data)
        {
            downPoint = data.position;
            downTime = Time.time;
        }
        private void HandlePointerUp(PointerEventData data)
        {
            var swipe = data.position - downPoint;
            var swipeNormalised = swipe / Screen.width;

            if (Time.time - downTime <= maxTime && swipeNormalised.magnitude > minDistanceNormalised)
            {
                overlay.MuteSingleFrame(UIEventType.PointerClick);
                swipeSubject.OnNext(swipeNormalised);
            }

            downPoint = Vector2.zero;
            downTime = 0;
        }
    }
}