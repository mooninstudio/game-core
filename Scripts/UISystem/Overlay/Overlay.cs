using UnityEngine;
using UnityEngine.UI;

namespace Moonin.UISystem
{   
    [RequireComponent(typeof(Graphic)), DisallowMultipleComponent]
    public class Overlay : OverlayBase
    {
        [SerializeField] private UIEventType interceptMask = 0;
        [SerializeField] private UIEventType consumeMask = 0;

        protected override UIEventType InterceptMask => interceptMask;
        protected override UIEventType ConsumeMask => consumeMask;
    }
}
