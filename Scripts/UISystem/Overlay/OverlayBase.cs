using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Moonin.UISystem
{   
	[RequireComponent(typeof(Graphic), typeof(RectTransform)), DisallowMultipleComponent]
	public abstract class OverlayBase : UIBehaviour,
		IPointerClickHandler,
		IPointerDownHandler,
		IPointerUpHandler,
		IDragHandler,
		IPointerEnterHandler,
		IPointerExitHandler
	{
		public IObservable<PointerEventData> PointerDragStream => pointerDragSubject.AsObservable();
		public IObservable<PointerEventData> PointerClickStream => pointerClickSubject.AsObservable();
		public IObservable<PointerEventData> PointerDownStream => pointerDownSubject.AsObservable();
		public IObservable<PointerEventData> PointerUpStream => pointerUpSubject.AsObservable();
		public IObservable<PointerEventData> PointerEnterStream => pointerUpSubject.AsObservable();
		public IObservable<PointerEventData> PointerExitStream => pointerUpSubject.AsObservable();
		public RectTransform RectTransform { get; private set; }

		protected abstract UIEventType InterceptMask { get; }
		protected abstract UIEventType ConsumeMask { get; }

		private Subject<PointerEventData> pointerDragSubject = new Subject<PointerEventData>();
		private Subject<PointerEventData> pointerClickSubject = new Subject<PointerEventData>();
		private Subject<PointerEventData> pointerDownSubject = new Subject<PointerEventData>();
		private Subject<PointerEventData> pointerUpSubject = new Subject<PointerEventData>();
		private Subject<PointerEventData> pointerEnterSubject = new Subject<PointerEventData>();
		private Subject<PointerEventData> pointerExitSubject = new Subject<PointerEventData>();

		private List<RaycastResult> raycastResults = new List<RaycastResult>();
		private GraphicRaycaster raycaster;
		private HashSet<UIEventType> singleFrameMutedEvents = new HashSet<UIEventType>();

		protected override void Awake()
		{
			raycaster = GetComponentInParent<GraphicRaycaster>();
			RectTransform = GetComponent<RectTransform>();
		}

		private void LateUpdate()
		{
			if(!singleFrameMutedEvents.Any())
				return;
				
			singleFrameMutedEvents.Clear();
		}

		public void MuteSingleFrame(params UIEventType[] events)
		{
			foreach (var e in events)
			{
				singleFrameMutedEvents.Add(e);
			}
		}

		public virtual void OnPointerClick(PointerEventData eventData) => HandleEvent<IPointerClickHandler>(eventData, UIEventType.PointerClick, pointerClickSubject, (h, d) => h.OnPointerClick(d));
		public virtual void OnPointerDown(PointerEventData eventData) => HandleEvent<IPointerDownHandler>(eventData, UIEventType.PointerDown, pointerDownSubject, (h, d) => h.OnPointerDown(d));
		public virtual void OnPointerUp(PointerEventData eventData) => HandleEvent<IPointerUpHandler>(eventData, UIEventType.PointerUp, pointerUpSubject, (h, d) => h.OnPointerUp(d));
		public virtual void OnDrag(PointerEventData eventData) => HandleEvent<IDragHandler>(eventData, UIEventType.Drag, pointerDragSubject, (h, d) => h.OnDrag(d));
		public virtual void OnPointerEnter(PointerEventData eventData) => HandleEvent<IPointerEnterHandler>(eventData, UIEventType.PointerEnter, pointerEnterSubject, (h, d) => h.OnPointerEnter(d));
		public virtual void OnPointerExit(PointerEventData eventData) => HandleEvent<IPointerExitHandler>(eventData, UIEventType.PointerExit, pointerExitSubject, (h, d) => h.OnPointerExit(d));

		private void HandleEvent<T>(PointerEventData eventData, UIEventType eventType, Subject<PointerEventData> eventSubject, Action<T, PointerEventData> eventHandlerFunctor) where T : IEventSystemHandler
		{
			if (InterceptMask.HasFlag(eventType))
				eventSubject.OnNext(eventData);
			if (!ConsumeMask.HasFlag(eventType) && !singleFrameMutedEvents.Contains(eventType))
				PropagateEvent(eventData, eventHandlerFunctor);
		}
		private void PropagateEvent<T>(PointerEventData eventData, Action<T, PointerEventData> handlerFunctor) where T : IEventSystemHandler
		{
			var target = GetTarget<T>(eventData);
			if (!target)
				return;

			ExecuteEvents.Execute<T>(target, eventData, (handler, data) => handlerFunctor(handler, data as PointerEventData));
		}
		private GameObject GetTarget<T>(PointerEventData eventData) where T : IEventSystemHandler
		{
			gameObject.SetActive(false);
			raycaster.Raycast(eventData, raycastResults);
			gameObject.SetActive(true);
			if (raycastResults.Count == 0)
				return null;

			var target = ExecuteEvents.GetEventHandler<T>(raycastResults.First().gameObject);
			raycastResults.Clear();
			return target;
		}
	}
}
