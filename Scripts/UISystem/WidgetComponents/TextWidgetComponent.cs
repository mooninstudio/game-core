using TMPro;
using UnityEngine;

namespace Moonin.UISystem
{   
    [System.Serializable]
    public class TextWidgetComponent
    {
        [SerializeField] private TextMeshProUGUI textField;

        public RectTransform Rect => textField.rectTransform;

        public void SetText(string text)
        {
            textField.text = text;
        }
        public void SetTextColor(Color color)
        {
            textField.color = color;
        }
        public void SetAlpha(float alpha)
        {
            var c = textField.color;
            textField.color = new Color(c.r, c.g, c.b, alpha);
        }
        public void SetTextSize(int size)
        {
            textField.fontSize = size;
        }
    }
}