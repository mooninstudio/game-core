using System;
using UnityEngine;
using UnityEngine.UI;

namespace Moonin.UISystem
{   
    [Serializable]
    public class ImageWidgetComponent
    {
        [SerializeField] private Image image;

        public void SetSprite(Sprite sprite)
        {
            image.sprite = sprite;
        }
        public void SetActive(bool isActive)
        {
            image.enabled = isActive;
        }
        public void SetColor(Color color)
        {
            image.color = color;
        }
        public void SetAlpha(float alpha)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
        }
    }
}