using UnityEngine;

namespace Moonin.UISystem
{   
    [System.Serializable]
    public class ColorWidgetComponent
    {
        [SerializeField] private Renderer image;

        public void SetColor(Color color)
        {
            image.material.color = color;
        }
    }
}