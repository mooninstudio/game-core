﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Moonin.UISystem
{   
	public class NavigationHandler : Selectable
	{
		public IObservable<Unit> SelectedStream => selectedSubject.AsObservable();
		public IObservable<Unit> DeselectedStream => deselectedSubject.AsObservable();

		private Subject<Unit> selectedSubject = new Subject<Unit>();
		private Subject<Unit> deselectedSubject = new Subject<Unit>();

		public override void OnDeselect(BaseEventData eventData)
		{
			base.OnDeselect(eventData);
			deselectedSubject.OnNext(Unit.Default);
		}

		public override void OnSelect(BaseEventData eventData)
		{
			base.OnSelect(eventData);
			selectedSubject.OnNext(Unit.Default);
		}
	}
}
