using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Moonin.UISystem
{   
	public class UIClickHandler : MonoBehaviour, IPointerClickHandler
	{
		public IObservable<Unit> ClickStream => clickSubject.AsObservable();
		private Subject<Unit> clickSubject = new Subject<Unit>();
		public void OnPointerClick(PointerEventData eventData) => clickSubject.OnNext(Unit.Default);
	}
}