﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Moonin.Resources
{
    public interface IRepository<out TResource>
    {
        TResource Get(Guid id);
        IEnumerable<TResource> GetAll();
        IEnumerable<TResource> FindAll(Func<TResource, bool> predicate);
    }
    
    public class Repository<TResource> : IRepository<TResource>
        where TResource : GuidResource
    {
        private Dictionary<Guid, TResource> _data;
        
        public Repository(string directory)
        {
            _data = UnityEngine.Resources
                .LoadAll<TResource>(directory)
                .ToDictionary(x => x.Id);
        }

        public TResource Get(Guid id) => _data[id];

        public IEnumerable<TResource> GetAll() => _data.Values;

        public IEnumerable<TResource> FindAll(Func<TResource, bool> predicate) => GetAll().Where(predicate);
    }
}