﻿using System;
using UnityEngine;
using Moonin.Extensions;

namespace Moonin.Resources
{
    public class GuidResource : ScriptableObject
    {
        [SerializeField, ReadOnly] private string _id;
        
        public Guid Id => Guid.Parse(_id);
        
        public void OnValidate()
        {
            _id = this.ReadAssetGuid();
        }
    }
}