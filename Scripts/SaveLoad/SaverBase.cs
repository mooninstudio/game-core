﻿using UnityEngine;
using System.IO;
using System;
using Newtonsoft.Json.Linq;

public abstract class SaverBase<T> where T : new()
{
    private string directoryPath;
    private string filePath;

    private const string FILE_VERSION_PROPERTY = "_fileVersion";

    ///<param name="directory">Path to save directory (e.g. Saves/Data).</param>
    ///<param name="file">Name of save file (without extension).</param>
    public SaverBase(string directory, string file)
	{
        directoryPath = Path.Combine(Application.persistentDataPath, directory);
        filePath = Path.Combine(directoryPath, file);
    }
	public T Load()
	{
		Debug.Log("Loading data...");
        T data;
        if (!File.Exists(filePath)) 
		{ 
			Debug.Log("Loaded default");
            data = GetStartingData();
		}
		else
		{
			using (StreamReader streamReader = File.OpenText(filePath))
			{
				string json = EncryptorDecryptor.EncryptDecrypt(streamReader.ReadToEnd());
                var saveObj = JObject.Parse(json);

                var fileVersion = saveObj[FILE_VERSION_PROPERTY]?.ToObject<int>() ?? 0;

                data = GetVersionDeserializer(fileVersion).Deserialize(saveObj);

				Debug.Log("Loaded from file: " + filePath);
				OnLoadedFromFile(data);
			}
		}
        return data;
	}
    protected virtual void OnLoadedFromFile(T data) { }
    protected virtual T GetStartingData() => new T()
	{
		
	};
    protected abstract IDeserializer<T> GetVersionDeserializer(int version);

    public void Save(T data, int version = 0)
	{
		Debug.Log("Saving data...");
        var jsonObj = JObject.Parse(JsonUtility.ToJson(data));
        jsonObj.Add(FILE_VERSION_PROPERTY, version);
        var dataJson = EncryptorDecryptor.EncryptDecrypt(jsonObj.ToString());

		if (!Directory.Exists(directoryPath)) Directory.CreateDirectory(directoryPath);
		using (StreamWriter streamWriter = File.CreateText(filePath))
		{
			streamWriter.Write(dataJson);
		}
		Debug.Log("Data saved!");
	}
	public void Delete()
	{
		if (!Directory.Exists(directoryPath) || !File.Exists(filePath))
		{
			Debug.Log("No data found.");
		}
		else
		{
			File.Delete(filePath);
			Debug.Log("Deleted save data.");
		}
	}
}
