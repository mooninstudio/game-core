using System;
using UnityEngine;

public abstract class SaveViewBase<TSave, TSaver> : MonoBehaviour
    where TSave : new()
    where TSaver : SaverBase<TSave>
{
    [Button("Delete")] protected void B_Delete() => Delete();
    [Button("Override")] protected void B_Override() => Override();
    [Button("Read")] protected void B_Read() => Read();
    [SerializeField] private TSave save;

    protected abstract string directory { get; }
    protected abstract string file { get; }

    private void Delete() => CreateSaver().Delete();
    private void Override() => CreateSaver().Save(save);
    private void Read() => save = CreateSaver().Load();
    private TSaver CreateSaver() => Activator.CreateInstance(typeof(TSaver), directory, file) as TSaver;
}