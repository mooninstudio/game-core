using Newtonsoft.Json.Linq;

public interface IDeserializer<out T>
{
    T Deserialize(JObject saveObject);
}