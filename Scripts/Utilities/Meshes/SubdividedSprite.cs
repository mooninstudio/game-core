using UnityEngine;
using System.Linq;

[RequireComponent(typeof(SpriteRenderer))]
public class SubdividedSprite : MonoBehaviour
{
    [SerializeField] private ushort subdivisionsX = 100;
    [SerializeField] private ushort subdivisionsY = 100;
    [SerializeField] private bool drawGizmos;

    private SpriteRenderer spriteRenderer;

    [Button("Subdivide")] public void Subdivide_Btn() => Subdivide();
    public void Start() => Subdivide();

    private void Subdivide()
    {
        spriteRenderer = GetComponent<SpriteRenderer>() ?? throw new System.NullReferenceException(nameof(spriteRenderer));

        var stepX = 1f / (subdivisionsX + 1) * spriteRenderer.sprite.pixelsPerUnit;
        var stepY = 1f / (subdivisionsY + 1) * spriteRenderer.sprite.pixelsPerUnit;
        var countX = subdivisionsX + 2;
        var countY = subdivisionsY + 2;

        var vertices = new Vector2[countX * countY];
        for (int y = 0; y < countY; y++)
            for (int x = 0; x < countX; x++)
                vertices[countX * y + x] = new Vector2(x * stepX, y * stepY);

        var count = (countX - 1) * (countY - 1);
        var triangles = new ushort[count * 6];
        for (int y = 0; y < countY - 1; y++)
            for (int x = 0; x < countX - 1; x++)
            {
                int v = y * countX + x;
                int i = y * (countX-1) + x;
                triangles[6 * i + 0] = (ushort)v;
                triangles[6 * i + 1] = (ushort)(v + countX);
                triangles[6 * i + 2] = (ushort)(v + 1);
                triangles[6 * i + 3] = (ushort)(v + 1);
                triangles[6 * i + 4] = (ushort)(v + countX);
                triangles[6 * i + 5] = (ushort)(v + countX + 1);
            }

        spriteRenderer.sprite.OverrideGeometry(vertices, triangles);
    }

    public void OnDrawGizmos()
    {
        if (!drawGizmos)
            return;

        var stepX = 1f / (subdivisionsX + 1);
        var stepY = 1f / (subdivisionsY + 1);
        var countX = subdivisionsX + 2;
        var countY = subdivisionsY + 2;

        var vertices = new Vector2[countX * countY];
        for (int y = 0; y < countY; y++)
            for (int x = 0; x < countX; x++)
                vertices[countX * y + x] = new Vector2(x * stepX, y * stepY);
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], .03f);
        }

        var count = (countX - 1) * (countY - 1);
        var triangles = new ushort[count * 6];
        for (int y = 0; y < countY - 1; y++)
            for (int x = 0; x < countX - 1; x++)
            {
                int v = y * countX + x;
                int i = y * (countX-1) + x;
                triangles[6 * i + 0] = (ushort)v;
                triangles[6 * i + 1] = (ushort)(v + countX);
                triangles[6 * i + 2] = (ushort)(v + 1);
                triangles[6 * i + 3] = (ushort)(v + 1);
                triangles[6 * i + 4] = (ushort)(v + countX);
                triangles[6 * i + 5] = (ushort)(v + countX + 1);
            }

        for (int i = 0; i < triangles.Length; i++)
        {
            if (i != 0)
            {
                Gizmos.DrawLine(vertices[triangles[i - 1]], vertices[triangles[i]]);
            }
        }
    }
}