﻿public class Buffer<T>
{
    public int Count { get; private set; }

    private T[] elements;
    private int capacity;

    public Buffer(int capacity)
    {
        Count = 0;
        this.capacity = capacity;
        elements = new T[capacity];
    }
    public void Push(T item)
    {
        MoveRight();
        elements[0] = item;
        if (Count < capacity) Count++;
    }
    public T this[int i]
    {
        get => elements[i];
        set => elements[i] = value;
    }
    public T[] ToArray()
    {
        return elements;
    }
    public void Clear()
    {
        for (int i = 0; i < capacity; i++)
            elements[i] = default;
        Count = 0;
    }
    
    private void MoveRight()
    {
        for (int i = capacity - 2; i >= 0; i--)
        {
            elements[i + 1] = elements[i];
        }
    }
}
