using System;
using System.Collections.Generic;
using System.Linq;

public class Pool<T>
{
    private readonly Func<T> constructor;
    private Queue<T> inactives;


    public Pool(Func<T> constructor)
    {
        this.constructor = constructor;
        inactives = new Queue<T>();
    }
    public T Use()
    {
        return inactives.Any() ? inactives.Dequeue() : constructor.Invoke();
    }
    public void Release(T item)
    {
        inactives.Enqueue(item);
    }
}