using System;
using System.Collections.Generic;
using UniRx;

public class ObservableList<T> : List<T>, IObservable<IEnumerable<T>>
    {
        private Subject<IEnumerable<T>> subject = new Subject<IEnumerable<T>>();
        public IDisposable Subscribe(IObserver<IEnumerable<T>> observer) => subject.Subscribe(observer);

        public IObservable<T> Added => addSubject.AsObservable();
        private Subject<T> addSubject = new Subject<T>();

        public IObservable<T> Removed => removeSubject.AsObservable();
        private Subject<T> removeSubject = new Subject<T>();

    public ObservableList(IEnumerable<T> collection) : base(collection) { }
    public ObservableList() { }

    public new void Add(T item)
        {
            base.Add(item);
            addSubject.OnNext(item);
            subject.OnNext(this);
        }

        public new void Remove(T item)
        {
            base.Remove(item);
            removeSubject.OnNext(item);
            subject.OnNext(this);
        }
    }