﻿using System;
using System.Collections.Generic;

public class DisposeBag : IDisposable
{
    private List<IDisposable> disposables;

    public DisposeBag()
    {
        disposables = new List<IDisposable>();
    }

    public void Add(IDisposable disposable)
    {
        this.disposables.Add(disposable);
    }
    public void Add(params IDisposable[] disposables)
    {
        this.disposables.AddRange(disposables);
    }
    public void Add(IEnumerable<IDisposable> disposables)
    {
        this.disposables.AddRange(disposables);
    }
    public void Dispose()
    {
        disposables.ForEach(disp => disp.Dispose());
        disposables.Clear();
    }
}
