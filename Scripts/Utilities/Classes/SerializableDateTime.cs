﻿using System;
using UnityEngine;

namespace Moonin.Utilities.Classes
{
    public class SerializableDateTime : ISerializationCallbackReceiver
    {
        public DateTime Value { get; set; }

        private string _value;

        public void OnBeforeSerialize()
        {
            _value = Value.ToLongDateString();
        }

        public void OnAfterDeserialize()
        {
            Value = DateTime.Parse(_value);
        }

        public static SerializableDateTime Now => new SerializableDateTime { Value = DateTime.Now };
    }
}