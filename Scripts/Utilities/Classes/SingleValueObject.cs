public abstract class SingleValueObject<TValue>
{
    public TValue Value { get; private set; }

    public SingleValueObject(TValue value)
    {
        this.Value = value;
    }

    public static bool operator ==(SingleValueObject<TValue> A, SingleValueObject<TValue> B) => A.Value.Equals( B.Value);
    public static bool operator != (SingleValueObject<TValue> A, SingleValueObject<TValue> B) => !A.Value.Equals( B.Value);
    public override bool Equals(object obj) {
        if(obj is SingleValueObject<TValue> singleValueObject)
            return this.Value.Equals(singleValueObject.Value);
        return false;
    }
    public override int GetHashCode() => Value.GetHashCode();
}