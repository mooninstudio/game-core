using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

public class Processor<TProcess, TEffect> where TProcess: IProcess<TEffect>
{
    public IObservable<TEffect> ProcessCompletedStream => processCompletedSubject.AsObservable();
    public IObservable<TProcess> ProcessStartingStream => processStartingSubject.AsObservable();

    private readonly IObservable<Unit> _clock;

    private Subject<TProcess> processStartingSubject = new Subject<TProcess>();
    private Subject<TEffect> processCompletedSubject = new Subject<TEffect>();
    private DisposeBag processingSubs;
    private Queue<TProcess> processesQueue;
    private TProcess activeProcess;
    private bool busy;

    public Processor(IObservable<Unit> clock)
    {
        _clock = clock;
        processesQueue = new Queue<TProcess>();
        processingSubs = new DisposeBag();
    }
    public void Push(TProcess process)
    {
        processesQueue.Enqueue(process);
        if(!busy)
            StartProcess(processesQueue.Dequeue());
    }
    public void Push(IEnumerable<TProcess> processes)
    {
        processesQueue.Enqueue(processes);
        if(!busy)
            StartProcess(processesQueue.Dequeue());
    }
    private void StartProcess(TProcess process)
    {
        busy = true;
        activeProcess = process;
        processStartingSubject.OnNext(process);
        processingSubs.Add(
            process.CompletedStream.Subscribe(OnProcessEnded),
            _clock.Subscribe(_ => process.Progress())
        );
    }
    private void OnProcessEnded(TEffect effect)
    {
        processingSubs.Dispose();
        activeProcess = default(TProcess);
        busy = false;
        processCompletedSubject.OnNext(effect);
        if(processesQueue.Any())
            StartProcess(processesQueue.Dequeue());
    }

    public void Clear()
    {
        processesQueue.Clear();
    }
}