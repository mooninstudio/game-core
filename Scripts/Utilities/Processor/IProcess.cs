using System;

public interface IProcess<TEffect>
{
    IObservable<TEffect> CompletedStream { get; }
    void Progress();
}