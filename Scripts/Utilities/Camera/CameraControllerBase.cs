﻿using System;
using UniRx;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public abstract class CameraControllerBase : MonoBehaviour, ICameraSettings
{
	public Rect ViewRect { get; private set; }
    public float ViewWidth => ViewRect.width;
    public float ViewHeight => ViewRect.height;
    public Vector3 Center => transform.position;
    public IObservable<Rect> FovChangedStream => fovCameraSubject.AsObservable();

    private float screenRatio;
	private new Camera camera;
	private Subject<Rect> fovCameraSubject = new Subject<Rect>();

	private void Awake()
	{
		screenRatio = (float)Screen.width / Screen.height;
		camera = GetComponent<Camera>();
        SetFovWidth(camera.orthographicSize);
    }

	public void SetCenter(Vector3 center)
	{
		transform.position = center;
	}
	public void SetFovWidth(float width)
	{
        var fov = ViewRect;
		fov.width = width * .5f;
		fov.height = fov.width / screenRatio;
		camera.orthographicSize = fov.height;
		ViewRect = fov;
		fovCameraSubject.OnNext(ViewRect);
	}
	public void SetFovHeight(float height)
	{
		var fov = ViewRect;
		fov.height = height * .5f;
		fov.width = fov.height * screenRatio;
		camera.orthographicSize = fov.height;
		ViewRect = fov;
		fovCameraSubject.OnNext(ViewRect);
	}

	public void FitFov(Vector2 size)
	{
		if (screenRatio > 1)
			SetFovHeight(size.y);
		else
			SetFovWidth(size.x);
	}
	public void SetBackgroundColor(Color color)
	{
		camera.backgroundColor = color;
	}
	public Vector3 GetOnEdgePosition(Vector3 from, Vector2Int direction)
    {
        return direction.y == 0 ?
            new Vector3(direction.x * ViewWidth + Center.x, from.y) :
            new Vector3(from.x, direction.y * ViewHeight + Center.y);
    }
	public IObservable<Unit> LerpFovWidth(float targetWidth, float duration)
	{
        var currentWidth = ViewWidth * 2;
        return CoroutineRunner.NewProcessCoroutine(x => SetFovWidth(Mathf.Lerp(currentWidth, targetWidth, x)), duration).AsObservable();
    }
	public IObservable<Unit> LerpFovHeight(float targetHeight, float duration)
	{
        var currentHeight = ViewHeight * 2;
        return CoroutineRunner.NewProcessCoroutine(x => SetFovHeight(Mathf.Lerp(currentHeight, targetHeight, x)), duration).AsObservable();
    }
	public IObservable<Unit> LerpCenter(Vector3 targetCenter, float duration)
	{
        var currentCenter = Center;
        return CoroutineRunner.NewProcessCoroutine(x => SetCenter(Vector3.Lerp(currentCenter, targetCenter, x)), duration).AsObservable();
    }
}
