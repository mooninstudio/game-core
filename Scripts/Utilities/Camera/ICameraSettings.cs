using System;
using UnityEngine;

public interface ICameraSettings
{
    Rect ViewRect { get; }
	float ViewWidth { get; }
    float ViewHeight { get; }
	Vector3 Center { get; }
    IObservable<Rect> FovChangedStream { get; }
    Vector3 GetOnEdgePosition(Vector3 from, Vector2Int direction);
}