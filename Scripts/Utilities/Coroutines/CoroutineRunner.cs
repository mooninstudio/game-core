﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using System;

public static class CoroutineRunner
{
    public static MonoBehaviour Mono { get { return mono ?? CreateCoroutineRunnerTargetObject(); } }
    private static MonoBehaviour mono;

    private static MonoBehaviour CreateCoroutineRunnerTargetObject()
    {
        mono = new GameObject("CoroutineRunner").AddComponent<EmptyMonoBehaviour>();
        return mono;
    }

    public static Coroutine Run(this IEnumerator coroutine)
    {
        return Mono.StartCoroutine(coroutine);
    }
    public static IObservable<Unit> AsObservable(this IEnumerator coroutine, bool publishEveryYield = false)
    {
        return Observable.FromCoroutine(() => coroutine, publishEveryYield);
    }
    public static IObservable<T> AsObservable<T>(this IEnumerator coroutine)
    {
        return Observable.FromCoroutineValue<T>(() => coroutine);
    }
    public static void Stop(this Coroutine coroutine)
    {
        if (coroutine != null) Mono.StopCoroutine(coroutine);
    }
    public static Coroutine DelayFrames(Action action, int delayFrames)
    {
        return DelayFramesCoroutine(action, delayFrames).Run();
    }
    private static IEnumerator DelayFramesCoroutine(Action action, int delayFrames)
    {
        for (int i = 0; i < delayFrames; i++)
            yield return null;
        action();
    }
    public static IEnumerator NewProcessCoroutine(Action<float> action, float duration, bool immediateStart = false, Action onComplete = null)
    {
        var progress = 0f;
        
        if(immediateStart)
            action(progress);

        while (progress != 1)
        {
            progress = Mathf.MoveTowards(progress, 1, Time.deltaTime / duration);
            yield return null;
            action(progress);
        }

        onComplete?.Invoke();
    }
    public static IEnumerator Concat(params IEnumerator[] coroutines)
    {
        foreach (var coroutine in coroutines)
            yield return coroutine;
    }

    public static IEnumerator EveryFrame(Action action, bool skipFrame = false)
    {
        if (skipFrame)
            yield return null;
        while (true)
        {
            action();
            yield return null;
        }
    }
}
namespace PausableCoroutines
{
    public class YieldInstruction
    {

        internal IEnumerator routine;

        internal YieldInstruction()
        {
        }

        internal virtual bool MoveNext()
        {

            var yieldInstruction = routine.Current as YieldInstruction;

            if (yieldInstruction != null)
            {
                if (yieldInstruction.MoveNext())
                {
                    return true;
                }
                else if (routine.MoveNext())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (routine.MoveNext())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class PausableCoroutine : YieldInstruction
    {
        public bool Paused { get; private set; }
        internal bool Stopped { get; private set; }

        internal PausableCoroutine(IEnumerator routine)
        {
            this.routine = routine;
        }
        internal override bool MoveNext()
        {
            if (Paused) return true;
            if (Stopped) return false;

            return base.MoveNext();
        }
        public void Pause() => Paused = true;
        public void Resume() => Paused = false;
        public void Stop() => Stopped = true;
    }
    public class Yielder
    {
        internal List<PausableCoroutine> coroutines = new List<PausableCoroutine>();

        internal PausableCoroutine Start(IEnumerator routine)
        {
            var coroutine = new PausableCoroutine(routine);
            coroutine.routine.MoveNext();
            coroutines.Add(coroutine);
            return coroutine;
        }
        internal void ProcessCoroutines()
        {
            for (int i = 0; i < coroutines.Count; i++)
            {
                var coroutine = coroutines[i];
                if (coroutine.MoveNext())
                {
                    ++i;
                }
                else if (coroutines.Count > 1)
                {
                    coroutines[i] = coroutines[coroutines.Count - 1];
                    coroutines.RemoveAt(coroutines.Count - 1);
                }
                else
                {
                    coroutines.Clear();
                    break;
                }
            }
        }
    }
    public static class CoroutineRunner
    {
        private static Yielder yielder;
        private static Yielder Yielder => yielder ?? CreateYielder();
        private static Yielder CreateYielder()
        {
            yielder = new Yielder();
            Observable.EveryUpdate().Subscribe(_ => yielder.ProcessCoroutines());
            return yielder;
        }

        public static PausableCoroutine Start(IEnumerator routine)
        {
            return Yielder.Start(routine);
        }
    }
    public static class IEnumeratorExtensions
    {
        public static PausableCoroutine RunPausable(this IEnumerator enumerator)
        {
            return CoroutineRunner.Start(enumerator);
        }
    }
}