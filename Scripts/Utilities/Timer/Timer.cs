using UnityEngine;

public class Timer
{
    private float? startTime;

    public void Start()
    {
        startTime = Time.time;
    }

    public float Elapsed => startTime.HasValue ? Time.time - startTime.Value : 0;
}