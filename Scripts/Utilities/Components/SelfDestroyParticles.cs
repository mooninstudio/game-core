﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class SelfDestroyParticles : MonoBehaviour
{
    private ParticleSystem particles;

    private void Awake()
    {
        particles = GetComponent<ParticleSystem>();
        SelfDestroy().Run();
    }
    private IEnumerator SelfDestroy()
    {
        while (particles.IsAlive()) yield return null;
        Destroy(gameObject);
    }
}
