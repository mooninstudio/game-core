﻿using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class SafeAreaUIScaler : MonoBehaviour
{
    [SerializeField] private RectTransform safeScreenArea;

    public void Awake()
    {
        Canvas canvas = GetComponent<Canvas>();
        Rect safeArea = Screen.safeArea;
        Vector2 anchorMin = safeArea.position;
        Vector2 anchorMax = safeArea.position + safeArea.size;
        anchorMin.x /= canvas.pixelRect.width;
        anchorMin.y /= canvas.pixelRect.height;
        anchorMax.x /= canvas.pixelRect.width;
        anchorMax.y /= canvas.pixelRect.height;

        safeScreenArea.anchorMin = anchorMin;
        safeScreenArea.anchorMax = anchorMax;
        safeScreenArea.sizeDelta = Vector2.zero;
    }
}
