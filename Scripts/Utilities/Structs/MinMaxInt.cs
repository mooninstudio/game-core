﻿using System;

[Serializable]
public struct MinMaxInt
{
	public int min;
	public int max;

	public int Random => UnityEngine.Random.Range(min, max+1);
}
