﻿using System;
using UnityEngine;

[Serializable, Flatten]
public class MinMax
{
	[SerializeField] private float min;
	[SerializeField] private float max;

	public float Min => min;
	public float Max => max;
	public float Random => UnityEngine.Random.Range(min, max);

	public MinMax(float min, float max)
	{
		this.min = min;
		this.max = max;
	}
	
	public float MapFromValue(float t)
	{
		return t.Map(0, 1, min, max);
	}
	public float MapToValue(float t)
	{
		return t.Map(min, max, 0, 1);
	}
}
