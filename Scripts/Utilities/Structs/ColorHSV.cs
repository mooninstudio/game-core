﻿using System;
using UnityEngine;

public class ColorHSV
{
    public float H = 0, S = 1, V = 1;
    public ColorHSV() { }
    public ColorHSV(float h, float s, float v)
    {
        H = h;
        S = s;
        V = v;
    }
    public ColorHSV(Color color)
    {
        Color.RGBToHSV(color, out H, out S, out V);
    }
    public Color ToRGB()
    {
    return Color.HSVToRGB(H, S, V);
    }
}
