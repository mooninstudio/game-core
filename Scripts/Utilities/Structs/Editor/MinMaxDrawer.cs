﻿using UnityEditor;
using UnityEngine;

namespace game_core.Scripts.Utilities.Structs.Editor
{
    [CustomPropertyDrawer(typeof(MinMax))]
    public class MinMaxDrawer : PropertyDrawer
    {
        private const int InputSpacing = 12;
        private const int InputLabelWidth = 32;
        
        public override void OnGUI(
            Rect position,
            SerializedProperty property,
            GUIContent label)
        {
            var minProperty = property.FindPropertyRelative("min");
            var maxProperty = property.FindPropertyRelative("max");
            var inputWidth = (position.width - EditorGUIUtility.labelWidth - InputSpacing) * .5f - InputLabelWidth;
            
            position.width = EditorGUIUtility.labelWidth;
            EditorGUI.LabelField(position, property.displayName);

            position.x += position.width;
            position.width = InputLabelWidth;
            EditorGUI.LabelField(position, minProperty.displayName);
            
            position.x += position.width;
            position.width = inputWidth;
            EditorGUI.PropertyField(position, minProperty, GUIContent.none);
            
            position.x += position.width + InputSpacing;
            position.width = InputLabelWidth;
            EditorGUI.LabelField(position, maxProperty.displayName);
            
            position.x += position.width;
            position.width = inputWidth;
            EditorGUI.PropertyField(position, maxProperty, GUIContent.none);
        }
    }
}