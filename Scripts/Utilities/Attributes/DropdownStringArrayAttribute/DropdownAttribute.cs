﻿using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class DropdownAttribute : PropertyAttribute
{
    public bool UseDefaultTagFieldDrawer = false;
    public string[] _options;
    private string _optionsPropertyName;

    public DropdownAttribute(string optionsPropertyName)
    {
        this._optionsPropertyName = optionsPropertyName;
    }

#if UNITY_EDITOR
    public string[] TryGetOptionsList(SerializedProperty property)
    {
        SerializedProperty optionsProperty = FindRelativeProperty(property, _optionsPropertyName);
        if (optionsProperty == null || !optionsProperty.isArray)
        {
            Debug.LogError("Wrong property name");
            return new string[] { };
        }
        string[] options = new string[optionsProperty.arraySize];
        for (int i = 0; i < optionsProperty.arraySize; i++)
            options[i] = optionsProperty.GetArrayElementAtIndex(i).stringValue;

        return options;
    }
    private SerializedProperty FindRelativeProperty(SerializedProperty property, string toGet)
    {
        if (property.depth == 0) return property.serializedObject.FindProperty(toGet);
        var path = property.propertyPath.Replace(".Array.data[", "[");
        var elements = path.Split('.');
        var nestedProperty = NestedPropertyOrigin(property, elements);

        if (nestedProperty != null) return null;
        return nestedProperty.FindPropertyRelative(toGet);
    }
    private SerializedProperty NestedPropertyOrigin(SerializedProperty property, string[] elements)
    {
        SerializedProperty parent = null;
        for (int i = 0; i < elements.Length - 1; i++)
        {
            var element = elements[i];
            int index = -1;
            if (element.Contains("["))
            {
                index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal))
                .Replace("[", "").Replace("]", ""));
                element = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
            }
            parent = i == 0
            ? property.serializedObject.FindProperty(element)
            : parent.FindPropertyRelative(element);
            if (index >= 0) parent = parent.GetArrayElementAtIndex(index);
        }
        return parent;
    }

    private string AsStringValue(SerializedProperty prop)
    {
        switch (prop.propertyType)
        {
            case SerializedPropertyType.String:
                return prop.stringValue;
            case SerializedPropertyType.Character:
            case SerializedPropertyType.Integer:
                if (prop.type == "char") return Convert.ToChar(prop.intValue).ToString();
                return prop.intValue.ToString();
            case SerializedPropertyType.ObjectReference:
                return prop.objectReferenceValue != null ? prop.objectReferenceValue.ToString() : "null";
            case SerializedPropertyType.Boolean:
                return prop.boolValue.ToString();
            case SerializedPropertyType.Enum:
                return prop.enumNames[prop.enumValueIndex];
            default:
                return string.Empty;
        }
    }
    readonly HashSet<object> _warningsPool = new HashSet<object>();
#endif
}
