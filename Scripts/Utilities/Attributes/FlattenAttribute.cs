﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using UnityEditor;

[AttributeUsage(AttributeTargets.Class)]
public class FlattenAttribute : PropertyAttribute
{
// #if UNITY_EDITOR
//     public bool ValidateProperty(SerializedProperty property)
//     {
//         return property != null && property.propertyType == SerializedPropertyType.String;
//     }
// #endif
}
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(FlattenAttribute))]
public class ComponentNameAttributeDrawer : PropertyDrawer
{
    private FlattenAttribute Attribute
    {
        get { return _attribute ?? (_attribute = attribute as FlattenAttribute); }
    }
    private FlattenAttribute _attribute;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        property.GetChildren().ToList().ForEach(p => Debug.Log(p.name));
        Debug.Log(property.GetChildren().Count());
        EditorGUI.PropertyField(position, property, label, true);
    }
}
#endif
