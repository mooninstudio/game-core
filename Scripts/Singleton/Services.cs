﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Moonin.Services
{
    public static class Services
    {
        private static Dictionary<Type, object> _services;

        public static void Initialize(params object[] services)
        {
            _services = services.ToDictionary(x => x.GetType());
        }
        
        public static TService Get<TService>() where TService : class
        {
            return _services[typeof(TService)] as TService;
        }
    }
}