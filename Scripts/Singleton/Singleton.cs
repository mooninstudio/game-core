﻿namespace Moonin.Resources
{
    public class Singleton<T>
        where T : class
    {
        public static T Instance => _instance ?? throw new SingletonNotYetCreatedException<T>();

        private static T _instance;
        
        public static void Set(T instance)
        {
            if (_instance != default)
                throw new SingletonAlreadyCreatedException<T>();
            _instance = instance;
        }
    }
}