﻿using System;

namespace Moonin.Resources
{
    public class SingletonAlreadyCreatedException<TSingleton> : Exception
    {
        public SingletonAlreadyCreatedException()
            : base($"Instance of {typeof(TSingleton).Name} has already been set.")
        {
            
        }
    }
}