﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Moonin.Resources
{
    public abstract class SingletonScriptableResource<TResource> : ScriptableObject
        where TResource : SingletonScriptableResource<TResource>
    {
        public static TResource Resource => _resource ??= Load();

        private static TResource _resource;
        private static readonly string ResourceName = typeof(TResource).Name;

        private static TResource Load()
        {
            var resource = UnityEngine.Resources.Load<TResource>(ResourceName);

            if (resource == null)
                throw new MissingResourceException(ResourceName);

            resource.OnLoaded();
            return resource;
        }

        protected virtual void OnLoaded()
        {
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (name == ResourceName)
                return;

            var assetPath = AssetDatabase.GetAssetPath(GetInstanceID());
            AssetDatabase.RenameAsset(assetPath, ResourceName);
        }
#endif
    }
}