﻿using System;

namespace Moonin.Resources
{
    public class SingletonNotYetCreatedException<TSingleton> : Exception
    {
        public SingletonNotYetCreatedException()
            : base($"Instance of {typeof(TSingleton).Name} has not been set yet.")
        {
            
        }
    }
}