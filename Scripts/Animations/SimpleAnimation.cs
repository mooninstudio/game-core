﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public class SimpleAnimation
{
    private GameObject animatedObject;
    private AnimationClip clip;
    private Coroutine animationCoroutine;
    private float speedMultiplier;
    private float tempSpeedMultiplier;

    public SimpleAnimation(GameObject animatedObject)
    {
        this.animatedObject = animatedObject;
    }
    public void Play(AnimationClip clip, bool reversed = false, float speedMultiplier = 1, Action onComplete = null)
    {
        Stop();
        SetSpeed(speedMultiplier);
        animationCoroutine = CoroutineRunner.Run(PlayAnimationCoroutine(clip, reversed, OnComplete: onComplete));
    }
    public void Loop(AnimationClip clip, bool reversed = false, float speedMultiplier = 1, float startProgress = 0, float duration = 0)
    {
        Stop();
        SetSpeed(speedMultiplier);
        animationCoroutine = CoroutineRunner.Run(LoopAnimationCoroutine(clip, reversed, startProgress, duration));
    }
    public void Pause()
    {
        speedMultiplier = 0;
    }
    public void Resume()
    {
        speedMultiplier = tempSpeedMultiplier;
    }
    public void Stop()
    {
        CoroutineRunner.Stop(animationCoroutine);
    }
    public void SetSpeed(float speedMultiplier)
    {
        this.speedMultiplier = speedMultiplier;
        tempSpeedMultiplier = speedMultiplier;
    }
    public void SampleAnimation(AnimationClip clip, float t, bool reversed = false){
        t = t.Clamp01();
        clip.SampleAnimation(animatedObject, (reversed ? 1 - t : t) * clip.length);
    }
    public IObservable<Unit> PlayAsObservable(AnimationClip clip, bool reversed = false, float speedMultiplier = 1, float? delay = null){
        Stop();
        SetSpeed(speedMultiplier);
        return Observable.FromCoroutine(() => PlayAnimationCoroutine(clip, reversed).Delay(delay ?? 0));
    }
    public IObservable<Unit> LoopAsObservable(AnimationClip clip, bool reversed = false, float speedMultiplier = 1, float? delay = null, float duration = 0){
        Stop();
        SetSpeed(speedMultiplier);
        return Observable.FromCoroutine(() => LoopAnimationCoroutine(clip, reversed, duration: duration).Delay(delay ?? 0));
    }

    private IEnumerator PlayAnimationCoroutine(AnimationClip clip, bool reversed, float? delay = null, Action OnComplete = null)
    {
        float progress = 0;
        do
        {
            progress = Mathf.MoveTowards(progress, 1, speedMultiplier * 1 / clip.length * Time.deltaTime);
            clip.SampleAnimation(animatedObject, (reversed ? 1 - progress : progress) * clip.length);
            yield return new WaitForEndOfFrame();
        }
        while (progress != 1);
        if (OnComplete != null) OnComplete.Invoke();
    }
    private IEnumerator LoopAnimationCoroutine(AnimationClip clip, bool reversed, float startProgress = 0, float duration = 0)
    {
        var progress = startProgress;
        var timer = duration;
        while (duration == 0 || timer > 0)
        {
            do
            {
                clip.SampleAnimation(animatedObject, (reversed ? 1 - progress : progress) * clip.length);
                progress = Mathf.MoveTowards(progress, 1, speedMultiplier * Time.deltaTime);
                timer = (timer - Time.deltaTime).Min(0);
                if(duration > 0 && timer == 0)
                    yield break;
                yield return null;
            }
            while (progress != 1);
            progress = 0;
        }
    }
}
